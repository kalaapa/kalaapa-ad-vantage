import fs from 'fs'
import path from 'path'

const pkg          = JSON.parse(fs.readFileSync('./package.json'))
const basePath     = path.join(__dirname, '..', 'web', 'app', 'themes', pkg.name)
const assetsPath   = path.join(basePath, 'assets-src')
const buildPath    = path.join(basePath, 'assets')
const env          = process.env.NODE_ENV || 'development'
const isProduction = env === 'production'

const config = {
  pkg: pkg,
  basePath: basePath,
  assetsPath: assetsPath,
  buildPath: buildPath,
  isProduction: isProduction,
  // JS (ES5)
  scriptsEs5: {
    order: 1,
    task: 'scripts-es5',
    enabled: true,
    revision: false,
    behavior: 'reload',
    src: path.join(assetsPath, 'js', 'es5', '**/*.js'),
    dest: path.join(buildPath, 'js')
  },
  // JS (ES6)
  scriptsEs6: {
    order: 1,
    task: 'scripts-es6',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: [
      path.join(assetsPath, 'js', 'es6', 'app.js'),
    ],
    dest: path.join(buildPath, 'js')
  },
  // CSS
  styles: {
    order: 10,
    task: 'styles',
    enabled: true,
    revision: true,
    behavior: 'inject',
    src: path.join(assetsPath, 'css', '**/*.scss'),
    dest: path.join(buildPath, 'css'),
    purgecss: {
      content: [
        `${basePath}/views/**/*.html.twig`,
        `${buildPath}/js/**/*.js`,
      ],
      whitelistPatterns: [
        /^mce-content-body?$/,
        /^current-menu-item?$/,
        /^textarea?$/,
      ],
      whitelistPatternsChildren: [
        /^editor(.*)?$/,
        /^bg(-.*)?$/,
        /^text(-.*)?$/,
        /^custom-select?$/,
      ]
    }
  },
  // SPRITE (SVG)
  spriteSvg: {
    order: 1,
    task: 'sprite-svg',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'images', 'sprite-svg', '*.svg'),
    dest: path.join(buildPath, 'images')
  },
  // IMAGES
  images: {
    order: 5,
    task: 'images',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: [
      `${assetsPath}/images/**/*.{jpg,jpeg,gif,png,svg}`,
      `!${assetsPath}/images/sprite-*/`,
      `!${assetsPath}/images/sprite-*/**`
    ],
    dest: path.join(buildPath, 'images')
  },
  // FONTS
  fonts: {
    order: 1,
    task: 'fonts',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'fonts', '*.{woff,woff2,eot,svg,ttf}'),
    dest: path.join(buildPath, 'fonts')
  },
  // MODERNIZR
  modernizr: {
    order: 1,
    task: 'modernizr',
    enabled: true,
    revision: true,
    behavior: 'reload',
    src: path.join(assetsPath, 'js', '**/*.js'),
    dest: path.join(buildPath, 'js'),
    options: {
      cache: true,
      crawl: false,
      parseFiles: false,
      customTests: [],
      tests: [
        'touchevents',
        'hovermq',
        'smil'
      ],
      options: [
        'setClasses'
      ]
    }
  }
}
export default config
