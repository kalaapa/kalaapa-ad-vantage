/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// STYLES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import config from '../config'
import plugins from '../utils/plugins'
import handleErrors from '../utils/handleErrors'
import gulp from 'gulp'
import path from 'path'

import packageImporter from 'node-sass-package-importer'
import autoprefixer from 'autoprefixer'
import postcssUrl from 'postcss-url'
import cssnano from 'cssnano'

export default () => {
  const filterCss = plugins.filter(['**/*.css'])
  const filterApp = plugins.filter(['**/app*.css'], {restore: true})
  const cloneSink = plugins.clone.sink();
  return gulp
    .src(config.styles.src)
    .pipe(plugins.if(!config.isProduction, plugins.appendPrepend.prependText('$debug:true;')))
    .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.init()))
    .pipe(plugins.sass({
      outputStyle: 'expanded',
      precision: 10,
      importer: packageImporter()
    }))
    .on('error', handleErrors)
    // RTL css
    // .pipe(cloneSink)
    // .pipe(plugins.clone())
    // .pipe(plugins.rename({ suffix: '-rtl' }))
    // .pipe(plugins.rtlcss())
    .pipe(cloneSink.tap())
    .pipe(plugins.postcss([
      autoprefixer,
      postcssUrl({
        basePath: path.join(path.resolve(config.buildPath), path.basename(config.buildPath)),
        url: 'inline',
        maxSize: 5,
        filter: '**/*.{jpg,jpeg,png,svg,gif}',
        optimizeSvgEncode: true
      })
    ]))
    .pipe(plugins.if(config.isProduction, plugins.postcss([
      cssnano({
        reduceTransforms: false,
        reduceIdents: false,
        discardComments: {
          removeAll: true
        }
      })
    ])))
    .pipe(filterApp)
    .pipe(plugins.if(config.isProduction && config.styles.hasOwnProperty('purgecss'), plugins.purgecss(config.styles.purgecss)))
    .pipe(filterApp.restore)
    .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.write('.')))
    .pipe(gulp.dest(config.styles.dest))
}
