/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// IMAGES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import config from '../config'
import plugins from '../utils/plugins'
import gulp from 'gulp'

import pngquant from 'imagemin-pngquant'
import zopfli from 'imagemin-zopfli'
import mozjpeg from 'imagemin-mozjpeg'

export default () => {
  return gulp.src(config.images.src)
    .pipe(plugins.newer(config.images.dest))
    .pipe(plugins.if(config.isProduction, plugins.imagemin([
      // GIF
      plugins.imagemin.gifsicle(),
      // JPEG
      mozjpeg({quality: 50}),
      // PNG
      pngquant({quality: [0.5, 0.5]}),
      // zopfli(),
      // SVG
      plugins.imagemin.svgo({
        plugins: [
          {doctypeDeclaration: false},
          {namespaceIDs: false},
          {xmlDeclaration: false},
          {removeViewBox: false},
          {cleanupIDs: {
            remove: true,
            minify: false
          }}
        ]
      })
    ])))
    .pipe(plugins.size({title: 'images'}))
    .pipe(gulp.dest(config.images.dest))
}
