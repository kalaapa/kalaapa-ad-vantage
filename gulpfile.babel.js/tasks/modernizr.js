/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// MODERNIZR
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import config from '../config'
import plugins from '../utils/plugins'
import gulp from 'gulp'

export default () => {
  return gulp
    .src(config.modernizr.src, { since: gulp.lastRun(config.modernizr.task) })
    .pipe(plugins.modernizr(config.modernizr.options))
    .pipe(plugins.uglify())
    .pipe(gulp.dest(config.modernizr.dest))
}
