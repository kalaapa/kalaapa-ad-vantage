/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// SPRITE SVG
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import gulp from 'gulp'
import config from '../config'
import plugins from '../utils/plugins'
import handleErrors from '../utils/handleErrors'

export default () => {
  return gulp
    .src(config.spriteSvg.src)
    .pipe(plugins.svgSprite({
      mode: {
        symbol: {
          sprite: 'sprite.svg',
          dest: '.'
        }
      },
      shape: {
        id: {
          generator: '%s'
        }
      },
      svg: {
        xmlDeclaration: false,
        doctypeDeclaration: false,
        namespaceIDs: false
      }
    }))
    .on('error', handleErrors)
    .pipe(gulp.dest(config.spriteSvg.dest))
}
