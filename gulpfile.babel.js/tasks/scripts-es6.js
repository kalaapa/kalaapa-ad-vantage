//////////////////////////////////////////////////////////////////////////////////////
/// SCRIPTS (ES6)
//////////////////////////////////////////////////////////////////////////////////////

import config from '../config'
import plugins from '../utils/plugins'
import gulp from 'gulp'

import betterRollup from 'gulp-better-rollup'
import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'

export default () => {
  return gulp
    .src(config.scriptsEs6.src)
    .pipe(plugins.sourcemaps.init())
    .pipe(betterRollup({
      cache: false,
      plugins: [
        resolve({
          browser: true,
        }),
        commonjs(),
        babel({
          babelrc: false,
          exclude: 'node_modules/**',
          presets: [
            [
              '@babel/preset-env',
              {
                // debug: true,
                corejs: 'core-js@3.1.3',
                useBuiltIns: 'usage'
              }
            ]
          ]
        })
      ]
    }, {
      format: 'iife',
    }))
    .pipe(plugins.if(config.isProduction, plugins.uglify()))
    // .pipe(plugins.if(config.isProduction, plugins.terser()))
    .pipe(plugins.if(!config.isProduction, plugins.sourcemaps.write('.')))
    .pipe(gulp.dest(config.scriptsEs6.dest))
}
