//////////////////////////////////////////////////////////////////////////////////////
/// SCRIPTS (ES5)
//////////////////////////////////////////////////////////////////////////////////////

import config from '../config'
import plugins from '../utils/plugins'
import gulp from 'gulp'

export default () => {
  return gulp
    .src(config.scriptsEs5.src)
    .pipe(plugins.uglify())
    .pipe(gulp.dest(config.scriptsEs5.dest))
}
