import path from 'path'
import gulp from 'gulp'
import config from '../config.js'

export function getEnabledTasks() {
  return Object.values(config).filter((task) => {
    if (!task.hasOwnProperty('task') || !task.hasOwnProperty('order') || !task.hasOwnProperty('enabled')) {
      return false
    }
    if (!task.enabled) {
      return false
    }
    return true
  })
}

export function getBuildTasks() {
  const tasks = getEnabledTasks()
  const tasksNames = {}
  tasks.forEach((task) => {
    if( !Array.isArray(tasksNames[task.order]) ) {
      tasksNames[task.order] = []
    }
    tasksNames[task.order].push(task.task)
  })
  return Object.values(tasksNames).map((subtasks) => gulp.parallel(subtasks))
}

export function getRevisionedTasks() {
  const tasks = getEnabledTasks()
  return Object.values(tasks).filter((task) => {
    if (!task.hasOwnProperty('revision')) {
      return false
    }
    if (!task.revision) {
      return false
    }
    return true
  })
}

export function getDestPaths(tasks = false) {
  tasks = tasks ? tasks : getEnabledTasks()
  const dests = []
  tasks.forEach((task) => {
    if (!task.hasOwnProperty('dest')) {
      return
    }
    dests.push(path.join(task.dest, '**/*'))
    dests.push(`!${path.join(task.dest, '**/*.map')}`)
  })
  return dests
}
