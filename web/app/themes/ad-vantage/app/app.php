<?php

namespace App;

use App\Core\Loader;
use App\Core\Provider\PathServiceProvider;
use App\Core\Provider\AssetServiceProvider;
use App\Core\Provider\TimberServiceProvider;
use App\Core\Provider\BreadcrumbServiceProvider;

function container() : \Pimple\Container
{
	static $container;
	if( ! $container ) {
		$container = new Loader();
	}
	return $container;
}

container()->register(new PathServiceProvider());

$assets_config = [
    'assets.base_urls' => [container()->get('paths.assets_url')],
    'assets.named_packages' => [
        'path' => [
            'base_path' => container()->get('paths.assets_path'),
        ]
    ]
];
if(!WP_DEBUG && in_array(WP_ENV, ['staging', 'production']) ) {
    $assets_config['assets.json_manifest_path'] = container()->get('paths.assets_path') . '/manifest.json';
    $assets_config['assets.named_packages']['path']['json_manifest_path'] = container()->get('paths.assets_path') . '/manifest.json';
}
container()->register(new AssetServiceProvider(), $assets_config);
container()->register(new TimberServiceProvider());
container()->register(new BreadcrumbServiceProvider());
