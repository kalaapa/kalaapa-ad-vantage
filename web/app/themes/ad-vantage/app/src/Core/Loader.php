<?php

namespace App\Core;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class Loader extends Container {

    protected $providers = [];
    protected $booted = false;

    public function __construct() {
        parent::__construct();
    }

    /**
     * Registers a service provider.
     *
     * @param ServiceProviderInterface $provider A ServiceProviderInterface instance
     * @param array                    $values   An array of values that customizes the provider
     *
     * @return Application
     */
    public function register(ServiceProviderInterface $provider, array $values = [])
    {
        $this->providers[] = $provider;
        parent::register($provider, $values);
        return $this;
    }

	/**
	 * Get item from container
	 *
	 * @param string $id
	 *
	 * @return mixed
	 */
	public function get( string $id )
	{
		return $this->offsetGet( $id );
	}
	/**
	 * Set item in container
	 *
	 * @param string $id
	 * @param mixed $value
	 */
	public function set( string $id, $value )
	{
		return $this->offsetSet( $id, $value );
    }

}
