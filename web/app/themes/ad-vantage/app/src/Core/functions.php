<?php

function container() : \Pimple\Container
{
	static $container;
	if( ! $container ){
		$container = new App\Core\Loader();
	}
	return $container;
}
