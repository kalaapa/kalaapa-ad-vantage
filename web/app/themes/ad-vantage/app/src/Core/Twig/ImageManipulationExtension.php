<?php

namespace App\Core\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;
use Timber\Image as TimberImage;

class ImageManipulationExtension extends AbstractExtension
{

    private $root_dir;
    private $base_target_dir;
    private $base_target_url;
    const CACHE_FOLDER = 'medias';

    public function __construct($app)
    {
        $this->root_dir = $app->get('paths.root_path');
        $this->base_target_dir = sprintf('%s/%s', $app->get('paths.upload_path'), $this::CACHE_FOLDER);
        $this->base_target_url = parse_url(sprintf('%s/%s', $app->get('paths.upload_url'), $this::CACHE_FOLDER), PHP_URL_PATH);
        if(!file_exists($this->base_target_dir)) {
            wp_mkdir_p($this->base_target_dir);
        }
    }

    public function getFilters()
    {
        return [
            new TwigFilter('manipulate', [$this, 'manipulate'], [
                'needs_context' => true,
            ]),
        ];
    }

    public function manipulate(array $context, $img, $operations)
    {

        if(! $img instanceof TimberImage) {
            return false;
        }

        $image_path = $img->file_loc;
        if(!is_file($image_path)) {
            return;
        }
        $operations = array_filter($operations);

        if(!empty($operations['datauri'])) {
            unset($operations['datauri']);
            unset($operations['srcset']);
            $path = $this->run($image_path, $operations, $context, 'path');
            $mime = wp_check_filetype($image_path);
            $data = file_get_contents($path);
            $base64 = sprintf(';%s', 'base64');
            $dataURI = base64_encode($data);
            return sprintf('data:%s%s,%s', $mime['type'], $base64, $dataURI);
        }

        // src
        if(empty($operations['srcset'])) {
            return $this->run($image_path, $operations, $context);
        }

        // srcset
        $set = $operations['srcset'];
        unset($operations['srcset']);
        $minWidth = $set['min'];
        $maxWidth = $set['max'];
        $step     = $set['step'] ?? 0.3;
        $widths    = $this->getSizes($img, $minWidth, $maxWidth, $step);

        $srcset = [];
        foreach($widths as $width) {
            $op = $operations;
            // width()
            if(!empty($operations['width'])) {
                $op['width'] = $width;
            }
            // crop()
            if(!empty($operations['crop'])) {
                $ratio = $op['crop']['height'] / $op['crop']['width'];
                $op['crop']['width'] = $width;
                $op['crop']['height'] = floor($ratio * $width);
            }
            $url = $this->run($image_path, $op, $context);
            $srcset[] = sprintf('%s %dw', $url, $width);
        }
        return implode(', ', $srcset);
    }

    public function run($path, $operations, $context, $return = 'url') {

        // Fallback to jpg if webp isn't supported
        if ( !empty($operations['format']) && $operations['format'] === 'webp') {
            $supports_webp = isset($context['support']['webp']) ? $context['support']['webp'] : false;
            if(!$supports_webp) {
                unset($operations['format']);
            }
        }

        // Create unique hash from image path/operations
        $path_relative = str_replace($this->root_dir, '', $path);
        $hash           = substr(md5(json_encode($operations).$path_relative), 0, 8);

        // Decompose image path
        $file_ext       = strtolower(str_replace('jpeg', 'jpg', pathinfo($path, PATHINFO_EXTENSION)));
        $file_name      = pathinfo($path, PATHINFO_FILENAME);

        $suffix = '';
        if(!empty($operations['crop'])) {
            $suffix = sprintf('-%d', $operations['crop']['width']);
        }
        if(!empty($operations['width'])) {
            $suffix = sprintf('-%d', $operations['width']);
        }

        // Dest file name
        $dest_file_name = sprintf('%s-%s%s.%s', $file_name, $hash, $suffix, $file_ext);
        $dest_file_url = sprintf('%s/%s', $this->base_target_url, $dest_file_name);
        $dest_file_path = sprintf('%s/%s', $this->base_target_dir, $dest_file_name);

        if(!empty($operations['format'])) {
            $dest_file_path = preg_replace(sprintf('/%s$/', $file_ext), $operations['format'], $dest_file_path);
            $dest_file_url = preg_replace(sprintf('/%s$/', $file_ext), $operations['format'], $dest_file_url);
        }

        if(is_file($dest_file_path)) {
            return $return == 'url' ? $dest_file_url : $dest_file_path;
        }

        // Increase memory limit
        wp_raise_memory_limit( 'image' );

        $image = Image::load($path);
        if(extension_loaded('imagick') && class_exists('Imagick')) {
            // $image->useImageDriver('imagick');
        }
        $optimize = false;
        foreach($operations as $operation => $value) {
            if(!method_exists(Manipulations::class, $operation)) {
                // @todo Maybe throw error
                // throw new \BadMethodCallException("Manipulation `{$operation}` does not exist");
                continue;
            }

            if($operation === 'optimize') {
                $optimize = true;
                continue;
            }
            if($operation === 'crop') {
                $image->$operation(Manipulations::CROP_CENTER, $value['width'], $value['height']);
            } elseif(is_bool($value)) {
                $image->$operation();
            } else {
                $image->$operation($value);
            }
            $image->apply();
        }
        if($optimize) {
            $image->optimize();
        }

        $image->save($dest_file_path);

        return $return == 'url' ? $dest_file_url : $dest_file_path;
    }

    public function getSizes(TimberImage $img, $minWidth, $maxWidth, $step = 0.5) {
        // $width = $img->width();
        // $height = $img->height();
        $sizes = [$minWidth];
        $width = $minWidth;
        while ($width >= $minWidth && $width <= $maxWidth) {
            $width = round($width * ($step + 1));
            // $height = floor($height * $step);
            if (!$maxWidth || $width <= $maxWidth) {
                $sizes[] = (int) $width;
            }
        }
        $sizes[] = $maxWidth;
        return $sizes;
    }

    public function getName()
    {
        return 'img_operation_extension';
    }
}
