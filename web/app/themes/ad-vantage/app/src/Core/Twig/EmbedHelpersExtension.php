<?php

namespace App\Core\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EmbedHelpersExtension extends AbstractExtension
{

    const YOUTUBE_DEFAULTS = [
        'autoplay'        => 0,
        'cc_lang_pref'    => null,
        'cc_load_policy'  => null,
        'color'           => 'red',
        'controls'        => 1,
        'disablekb'       => 0,
        'enablejsapi'     => 0,
        'end'             => null,
        'fs'              => 1,
        'hl'              => null,
        'iv_load_policy'  => 1,
        'list'            => null,
        'listType'        => null,
        'loop'            => 0,
        'modestbranding'  => 0,
        'origin'          => null,
        'playlist'        => null,
        'playsinline'     => 0,
        'rel'             => 1,
        'start'           => null,
        'widget_referrer' => null,
    ];

    public function getFilters()
    {
        return [
            new TwigFilter('youtube_args', [$this, 'addParameters']),
        ];
    }

    /**
     * Add parameters
     *
     * @see https://developers.google.com/youtube/player_parameters#Parameters
     * @param array $params
     * @return string
     */
    public function addParameters($iframe, $params = [], $attribute = 'src|data-src')
    {

        if(empty($params)) {
            return $iframe;
        }

        preg_match('/(' . $attribute . ')="([https?\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}\/\S*)?"/i', $iframe, $matches);

        if(empty($matches[2])) {
            return $iframe;
        }

        $embed_url = $matches[2];

        $url_components = parse_url($embed_url);

        // $params = array_merge($this::DEFAULTS, $params);
        $params = array_filter($params, function($value, $key) {
            return in_array($key, array_keys($this::YOUTUBE_DEFAULTS), true);
        }, ARRAY_FILTER_USE_BOTH);

        if(empty($params)) {
            return $iframe;
        }

        if(!empty($url_components['query'])) {
            parse_str($url_components['query'], $query);
            $params = $query + $params;
        }

        $url_components['query'] = http_build_query($params);

        return str_replace($embed_url, $this->build_url($url_components), $iframe);
    }

    private function build_url(array $parts) {
        return (isset($parts['scheme']) ? "{$parts['scheme']}:" : '') .
            ((isset($parts['user']) || isset($parts['host'])) ? '//' : '') .
            (isset($parts['user']) ? "{$parts['user']}" : '') .
            (isset($parts['pass']) ? ":{$parts['pass']}" : '') .
            (isset($parts['user']) ? '@' : '') .
            (isset($parts['host']) ? "{$parts['host']}" : '') .
            (isset($parts['port']) ? ":{$parts['port']}" : '') .
            (isset($parts['path']) ? "{$parts['path']}" : '') .
            (isset($parts['query']) ? "?{$parts['query']}" : '') .
            (isset($parts['fragment']) ? "#{$parts['fragment']}" : '');
    }

    public function getName()
    {
        return 'embedhelpers_extension';
    }
}
