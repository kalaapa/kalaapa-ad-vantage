<?php

namespace App\Core\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ImageHelpersExtension extends AbstractExtension
{

    public function getFunctions()
    {
        return [
            new TwigFunction('img_placeholder', [$this, 'imgPlaceholder']),
            new TwigFunction('width', [$this, 'width']),
            new TwigFunction('height', [$this, 'height']),
        ];
    }

    public function width($height_target, $width, $height)
    {
        return round(($width / $height) * $height_target);
    }

    public function height($width_target, $width, $height)
    {
        return round(($width / $height) * $width_target);
    }

    public function imgPlaceholder($width, $height, $fill = null)
    {
        $style = $fill ? sprintf('style="background:%s"', $fill) : '';
        $svg = <<<SVG
<svg xmlns="http://www.w3.org/2000/svg" width="{$width}" height="{$height}" {$style}></svg>
SVG;
        return sprintf("data:image/svg+xml,%s", $this->encodeOptimizedSVGDataUri($svg));
    }

    public function encodeOptimizedSVGDataUri(string $uri)
    {
        // First, uri encode everything
        $uri = rawurlencode($uri);
        $replacements = [
            // remove newlines
            '/%0A/' => '',
            // put spaces back in
            // '/%20/' => ' ',
            // put equals signs back in
            '/%3D/' => '=',
            // put colons back in
            '/%3A/' => ':',
            // put slashes back in
            '/%2F/' => '/',
            // replace quotes with apostrophes (may break certain SVGs)
            '/%22/' => "'",
        ];
        foreach ($replacements as $pattern => $replacement) {
            $uri = preg_replace($pattern, $replacement, $uri);
        }
        return $uri;
    }

    public function getName()
    {
        return 'imgPlaceholder_extension';
    }
}
