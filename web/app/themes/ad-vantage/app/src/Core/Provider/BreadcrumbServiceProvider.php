<?php

namespace App\Core\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class BreadcrumbServiceProvider implements ServiceProviderInterface {

    public function register(Container $app)
    {
        $app['app.breadcrumb'] = function() {
            if( !class_exists('\Inc2734\WP_Breadcrumbs\Bootstrap') ) {
                return [];
            }
            add_filter('inc2734_wp_breadcrumbs_remove_last_link', '__return_false');
            $breadcrumbs = new \Inc2734\WP_Breadcrumbs\Bootstrap();
            return $breadcrumbs->get();
        };
    }

}
