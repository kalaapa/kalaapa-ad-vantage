<?php

namespace App\Core\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use App\Core\Twig\AssetExtension;
use App\Core\Twig\ImageHelpersExtension;
use App\Core\Twig\EmbedHelpersExtension;
use App\Core\Twig\ImageManipulationExtension;
use voku\helper\HtmlMin;
use voku\twig\MinifyHtmlExtension;
use Twig\TwigFilter;

class TimberServiceProvider implements ServiceProviderInterface
{

    private $conditional_tags = [
        'home',
        'front_page',
        'category',
        'single',
        'singular',
        'page',
        'tax',
        'author',
        'search',
        '404',
        'paged',
        'attachment',
        'preview',
        'rtl',
    ];

    public function register(Container $app)
    {

        $this->app = $app;

        add_filter('timber/loader/twig', function($twig) use ($app) {
            // Asset extension
            if (isset($app['assets.packages'])) {
                $twig->addExtension( new AssetExtension($app->get('assets.packages')) );
            }
            return $twig;
        });

        // Image path
        add_filter( 'timber/image/new_path', function($path) use ($app) {
            $base_path = $app->get('paths.upload_path') . '/cache';
            $replace = '';
            // cache already
            if( false !== strpos($path, $base_path) ) {
                return $path;
            }
            // uploads
            if( false !== strpos($path, $app->get('paths.upload_path')) ) {
                $replace = $app->get('paths.upload_path');
            }
            // assets
            if( false !== strpos($path, $app->get('paths.assets_path')) ) {
                $replace = $app->get('paths.assets_path');
            }
            return $base_path . str_replace($replace, '', $path);
        });

        // Image URL
        add_filter('timber/image/new_url', function($url) use ($app) {
            $base_url = $app->get('paths.upload_url') . '/cache';
            $replace = '';
            if( false !== strpos($url, $base_url) ) {
                return $url;
            }
            // uploads
            if( false !== strpos($url, $app->get('paths.upload_url')) ) {
                $replace = $app->get('paths.upload_url');
            }
            // assets
            if( false !== strpos($url, $app->get('paths.assets_url')) ) {
                $replace = $app->get('paths.assets_url');
            }
            return $base_url . str_replace($replace, '', $url);
        });

        // Cache location
        add_filter( 'timber/cache/location', function( $location ) use ($app) {
            return $app->get('paths.project_path') . '/var/cache/twig';
        });

        // Twig
        add_filter('timber/loader/twig', [$this, 'configure_twig']);

        add_filter('timber/twig', function($twig) {
            $twig->addFilter(new TwigFilter('array_filter', 'twig_array_filter'));
            return $twig;
        });

        add_filter('timber/context', function($context) {
            // Add env
            $context['debug'] = WP_DEBUG;
            $context['env'] = defined('WP_ENV') ? WP_ENV : false;

            // Add locale
            global $wp_locale;
            $context['locale'] = $wp_locale;

            // webp
            if(extension_loaded('imagick') && class_exists('Imagick')) {
                // $context['support']['webp'] = !empty(\Imagick::queryFormats('WEBP'));
            } else {
                // $context['support']['webp'] = function_exists('imagewebp');
            }
            $context['support']['webp'] = function_exists('imagewebp');

            // Template conditional
            foreach ( $this->conditional_tags as $tag ) {
                $name = 'is_' . $tag;
                $context[ $name ] = $name();
            }

            return $context;
        });
    }


    public function configure_twig($twig) {
        $twig = $this->add_extensions($twig);
        $twig = $this->format_numbers($twig);
        return $twig;
    }

    protected function add_extensions($twig) {
        $twig->addExtension( new ImageHelpersExtension() );
        $twig->addExtension( new EmbedHelpersExtension() );
        $twig->addExtension( new ImageManipulationExtension($this->app) );
        $minifier = new HtmlMin();
        $twig->addExtension(new MinifyHtmlExtension($minifier));
        return $twig;
    }

    protected function format_numbers( $twig ) {
        global $wp_locale;

        if ( !isset( $wp_locale ) ) {
            return $twig;
        }

        $twig->getExtension('Twig_Extension_Core')->setNumberFormat(0, $wp_locale->number_format['decimal_point'], $wp_locale->number_format['thousands_sep']);
        return $twig;
    }

}
