<?php

namespace App\Core\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PathServiceProvider implements ServiceProviderInterface {

    public function register(Container $app)
    {
        $app['paths.project_path'] = function () {
            $abspath = untrailingslashit(ABSPATH);
            if( is_file( $abspath . '/../index.php' ) ) {
                return realpath($abspath . '/../..');
            } elseif( is_file( $abspath . '/index.php' ) ) {
                return dirname($abspath);
            }
            return null;
        };
        $home_url = home_url('/');
        $app['paths.home_root_url'] = function () use ($home_url) {
            return $home_url;
        };
        $app['paths.root_path'] = function () use ($home_url) {
            $abspath = untrailingslashit(ABSPATH);
            if( is_file( $abspath . '/../index.php' ) ) {
                return realpath($abspath . '/..');
            } elseif( is_file( $abspath . '/index.php' ) ) {
                return realpath($abspath);
            }
            return null;
        };
        $app['paths.upload_url'] = function () {
            $upload = wp_get_upload_dir();
            return empty($upload['error']) ? $upload['baseurl'] : null;
        };
        $app['paths.upload_path'] = function () {
            $upload = wp_get_upload_dir();
            return empty($upload['error']) ? $upload['basedir'] : null;
        };
        $app['paths.theme_url'] = function () {
            return untrailingslashit(get_template_directory_uri());
        };
        $app['paths.theme_path'] = function () {
            return untrailingslashit(get_template_directory());
        };
        $app['paths.assets_url'] = function ($app) {
            return $app->get('paths.theme_url') . '/assets';
        };
        $app['paths.assets_path'] = function ($app) {
            return $app->get('paths.theme_path') . '/assets';
        };
    }

}
