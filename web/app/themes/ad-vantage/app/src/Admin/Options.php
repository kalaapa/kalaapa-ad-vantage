<?php

namespace App\Admin;

class Options {

    const SLUG_JOB = 'advantage-job-settings';
    const SLUG_GLOBAL = 'advantage-settings';

    public function __construct() {
        if( !is_admin() ) {
            return;
        }
        if( !function_exists('acf_add_options_sub_page') ) {
            return;
        }

        acf_add_options_sub_page([
            'menu_slug'   => $this::SLUG_JOB,
            'page_title'  => 'Configuration',
            'menu_title'  => 'Configuration',
            'parent_slug' => 'edit.php?post_type=job',
        ]);

        acf_add_options_sub_page([
            'menu_slug'   => $this::SLUG_GLOBAL,
            'page_title'  => 'Configuration',
            'menu_title'  => 'Configuration',
            'parent_slug' => 'options-general.php',
            'autoload'    => true,
        ]);
    }

}
