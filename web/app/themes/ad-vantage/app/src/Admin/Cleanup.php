<?php

namespace App\Admin;

use function Sober\Intervention\intervention;

class Cleanup {

    public function __construct() {
        if(!is_admin()) {
            return;
        }

        // Remove post tag and posts from nav menus
        add_filter('nav_menu_meta_box_object', [$this, 'clean_nav_menus']);
        add_action('init', [$this, 'clean_admin']);
        add_action('admin_init', [$this, 'remove_language_switcher'], 100);
        if(!WP_DEBUG) {
            add_filter('acf/settings/show_admin', '__return_false');
        }

        add_action('admin_menu', function() {
            remove_submenu_page('loco', 'loco-core');
            remove_submenu_page('loco', 'loco-plugin');
            remove_submenu_page('loco', 'loco-lang');
        }, 30);
    }

    public function remove_language_switcher() {
        if(!function_exists('PLL')) {
            return;
        }
        $pll = PLL();
        remove_meta_box( 'pll_lang_switch_box', 'nav-menus', 'side');
    }

    public function clean_admin() {
        if ( !function_exists( '\Sober\Intervention\intervention' ) ) {
            return;
        }
        // intervention('remove-menu-items', ['posts'], 'all');
        intervention('remove-emoji');
        intervention('remove-dashboard-items', ['activity', 'incoming-links', 'plugins', 'quick-draft', 'drafts', 'news'], 'all');
        intervention( 'remove-howdy', 'Bonjour' );
    }

    public function clean_nav_menus($obj) {
        if ( in_array( $obj->name, [ 'post', 'post_tag' ], true ) ) {
            return false;
        }
        return $obj;
    }
}
