<?php

namespace App\Admin;

use App\Theme\Colors;
use App\PostTypes;

class Editor {

    public function __construct() {
        // Remove extra top space on front
        add_action('template_redirect', function() {
            remove_action('wp_head', '_admin_bar_bump_cb');
        });
        if( !is_admin() ) {
            // Remove Gutenberg default styles
            remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
        }

        // Add custom colors
        add_filter('tiny_mce_before_init', [$this, 'add_text_colors'] );
        // Remove h1
        add_filter( 'tiny_mce_before_init', [$this, 'remove_h1'] );

        // Enhance select2 color selector
        add_action('acf/input/admin_head', [$this, 'color_selector']);

        // Add hint after job title
        add_action('edit_form_after_title', function() {
            if( get_post_type() !== PostTypes::POST_TYPE_JOB ) {
                return;
            }
            echo '<div><span class="description">ℹ️ Intitulé du poste (et non de l’offre d’emploi), tel que "ingénieur logiciel" ou "chef de partie".</span></div>';
        });
    }

    public function remove_h1( $init ) {
        $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;';
        return $init;
    }

    public function add_text_colors( $init ) {
        $colors = Colors::get_colors();
        $color_map = [];
        foreach($colors as $color) {
            $color_map[] = str_replace('#', '', $color['color']);
            $color_map[] = $color['label'];
        }

        $init['textcolor_map'] = '["' . implode('", "', $color_map) . '"]';
        return $init;
    }

    public function color_selector() {
        ?>
        <script>
        (function($){
            if( typeof acf.addFilter === undefined ) {
                return;
            }
            acf.addFilter('select2_args', function( args, $select, settings, field, instance ) {
                if (!instance.$el.hasClass('js-color-select')) {
                    return args;
                }
                var colors = <?php echo json_encode(Colors::get_colors('color')); ?>;

                var template = function(state) {
                    if (!state.id) {
                        return state.text;
                    }
                    return $('<span style="border:solid 1px #ccc;display:inline-block;position:relative;width:12px;height:12px;top:1px;margin-right:4px;background:' + colors[state.id] + '"></span> <span>' + state.text + ' <span style="color:#999">(' + colors[state.id] + ')</span></span>');
                }

                args.templateResult = template;
                args.templateSelection = template;

                return args;
            });
        })(jQuery);
        </script>
        <?php
    }

}
