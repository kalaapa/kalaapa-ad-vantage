<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_global_forms', [
    'title'                 => 'Clés reCAPTCHA',
    'instruction_placement' => 'field',
]);

$fields
    ->addText('recaptcha_site_key')
        ->setConfig('label', 'Clé du site')
        ->setInstructions('Créer des clés gratuitement en suivant <a href="https://g.co/recaptcha/v3" target="_blank" rel="noopener">ce lien</a>.')
    ->addText('recaptcha_site_secret')
        ->setConfig('label', 'Clé secrète')
    ->addEmail('contact_email_to')
        ->setConfig('label', 'Email destinataire')
        ->setInstructions('Saississez le ou les emails qui recevront les messages du formulaire de contact. Par défaut, les messages sont envoyés à l’administrateur du site.')
    ->setLocation('options_page', '==', Options::SLUG_GLOBAL)
;

return $fields;
