<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_job_services', [
    'title' => 'Services tiers',
    'instruction_placement' => 'field',
]);
$fields
    ->addText('job_front_google_api_key')
        ->setConfig('label', 'Clé d’API Google (front)')
        ->setInstructions('
            Les API suivantes doivent être activées pour cette clé : Geocoding API, Maps JavaScript API, Places API<br />
            Cette clé possèdera une restriction de type HTTP.
        ')
    ->addText('job_back_google_api_key')
        ->setConfig('label', 'Clé d’API Google (back)')
        ->setInstructions('
            Les API suivantes doivent être activées pour cette clé : Geocoding API, Places API<br />
            Cette clé possèdera une restriction de type IP.
        ')
    ->setLocation('options_page', '==', Options::SLUG_JOB)
;

return $fields;
