<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_global_social', [
    'title'                 => 'Réseaux sociaux',
    'instruction_placement' => 'field',
]);

$fields
    ->addUrl('linkedin_url')
        ->setConfig('label', 'URL LinkedIn')
    ->addUrl('viadeo_url')
        ->setConfig('label', 'URL Viadeo')
    ->setLocation('options_page', '==', Options::SLUG_GLOBAL)
;

return $fields;
