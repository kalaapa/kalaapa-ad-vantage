<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Theme\Colors;

$fields = new FieldsBuilder('page_theme', [
    'title'                 => 'Thème de la page',
    'position'              => 'side',
    'instruction_placement' => 'field',
    'menu_order'            => 10,
]);

$fields
    ->addSelect('primary_color', [
        'ui'            => 1,
        'class'         => 'js-color-select',
        'allow_null'    => 0,
    ])
        ->setConfig('label', 'Couleur dominante')
        ->addChoices(Colors::get_colors('label'))
    ->setLocation('post_type', '==', 'page')
;

return $fields;
