<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_job_apply', [
    'title' => 'Formulaire',
    'instruction_placement' => 'field',
]);
$fields
    ->addEmail('job_email_to')
        ->setConfig('label', 'Email destinataire')
        ->setRequired()
        ->setInstructions('Saississez l’email qui recevra les candidatures. Par défaut, les messages sont envoyés à l’administrateur du site.')
    ->setLocation('options_page', '==', Options::SLUG_JOB)
;

return $fields;
