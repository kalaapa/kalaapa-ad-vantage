<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_global_tracking', [
    'title'                 => 'Statistiques & tracking',
    'instruction_placement' => 'field',
]);

$fields
    ->addText('google_analytics_id')
        ->setConfig('label', 'ID Google Analytics')
        ->setInstructions('Renseignez un ID Google Analytics du type UA-XXXXX-Y')
    ->setLocation('options_page', '==', Options::SLUG_GLOBAL)
;

return $fields;
