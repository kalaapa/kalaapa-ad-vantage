<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;

$position = require('fields/position.php');
$background_color = require('fields/background-color.php');
$content = require('fields/content.php');

$fields = new FieldsBuilder('blocks_content', [
    'title' => 'Blocs',
]);

$fields
    ->addFlexibleContent('blocks', [
        'label' => 'Blocs',
        'button_label' => 'Ajouter un bloc',
    ])
        // EDITOR
        ->addLayout('text', ['label' => 'Texte'])
            ->addWysiwyg('content')
                ->setConfig('label', 'Contenu')
        // TEXT | TEXT
        ->addLayout('text_text', ['label' => 'Texte | Texte'])
            ->addWysiwyg('content_left')
                ->setConfig('label', 'Contenu gauche')
                ->setConfig('media_upload', false)
            ->addWysiwyg('content_right')
                ->setConfig('label', 'Contenu droite')
                ->setConfig('media_upload', false)
        // TEXT | BOX
        ->addLayout('text_box', ['label' => 'Texte | Encadré'])
            ->addWysiwyg('content')
                ->setConfig('label', 'Contenu')
                ->setConfig('media_upload', false)
            ->addFields($position)
            ->addWysiwyg('content_box')
                ->setConfig('label', 'Contenu de l’encadré')
                ->setConfig('media_upload', false)
        // TEXT | IMAGE
        ->addLayout('text_image', [
            'label' => 'Texte | Image',
        ])
            ->addFields($content)
            ->addFields($position)
            ->addImage('image', ['label' => 'Image'])
        // TEXT | COVER
        // ->addLayout('text_cover', [
        //     'label' => 'Texte de couverture'
        // ])
        //     ->addFields($content)
        //     ->addLink('link', ['label' => 'Bouton'])
        //     ->addImage('img', ['label' => 'Image'])
        // COLUMNS
        ->addLayout('columns', [
            'label' => 'Blocs 3 colonnes'
        ])
            ->addRepeater('items', [
                'layout' => 'row',
            ])
                ->addImage('image')
                    ->setConfig('label', 'Image')
                ->addFields($content)
            ->endRepeater()
        // GALLERY
        ->addLayout('gallery', [
            'label' => 'Galerie d’images'
        ])
            ->addFields($background_color)
            ->addGallery('images', ['label' => 'Images'])
        // GALLERY LOGOS
        ->addLayout('gallery_logos', [
            'label' => 'Galerie de logos'
        ])
            ->addGallery('images', ['label' => 'Images'])
        // TIMELINE
        ->addLayout('timeline', [
            'label' => 'Timeline',
        ])
            ->addRepeater('items', [
                'button_label' => 'Ajouter un item',
                'layout' => 'row',
            ])
                ->addFields($content)
            ->endRepeater()
    ->endFlexibleContent()
    ->setLocation('post_type', '==', 'page')
        ->or('post_type', '==', 'post')
        ->and('page_type', '!=', 'front_page')
;

return $fields;
