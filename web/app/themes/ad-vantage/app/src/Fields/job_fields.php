<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\PostTypes;

$fields = new FieldsBuilder('job_meta', [
    'title'                 => 'Informations sur l’offre',
    'instruction_placement' => 'field',
]);

$fields
    ->addText('hiringOrganization')
        ->setRequired()
        ->setConfig('label', 'Nom de l’entreprise')
        ->setInstructions('ℹ️ Entreprise qui propose le poste. Il doit s’agir du nom de l’entreprise (par exemple, "Starbucks, Inc") et non de l’établissement spécifique qui recrute (par exemple, "Starbucks Paris Gare de l’Est").')
    ->addRepeater('locations', [
        'label' => 'Localisations',
        'min' => 1,
        'button_label' => 'Ajouter une localisation',
    ])
        ->addGoogleMap('jobLocation')
            ->setRequired()
            ->setConfig('label', 'Localisation')
            ->setInstructions('ℹ️ Le ou les sites de l’entreprise où l’employé se rendra au travail (par exemple, un bureau ou un lieu de travail), et non pas l’emplacement où l’offre d’emploi a été publiée. Incluez autant de champs que possible.')
    ->endRepeater()
    ->addDateTimePicker('validThrough')
        ->setRequired()
        ->setConfig('label', 'Date d’expiration de l’offre')
    ->addText('identifier')
        ->setConfig('label', 'ID de l’offre')
        ->setInstructions('ℹ️ Identifiant unique attribué à l’offre d’emploi par l’entreprise qui recrute.')
    ->addCheckbox('employmentType')
        ->addChoices([
            'FULL_TIME'  => 'Temps plein',
            'PART_TIME'  => 'Temps partiel',
            'CONTRACTOR' => 'Freelance',
            'TEMPORARY'  => 'Saisonnier',
            'INTERN'     => 'Stage',
            'VOLUNTEER'  => 'Bénévole',
            'PER_DIEM'   => 'Indemnité journalière',
            'OTHER'      => 'Autre',
        ])
        ->setConfig('label', 'Type d’emploi')
        ->setConfig('return_format', 'array')
    ->addWysiwyg('qualifications')
        ->setConfig('label', 'Qualifications')
    ->addText('educationRequirements')
        ->setConfig('label', 'Niveau d’étude')
        ->setInstructions('ℹ️ Niveau d’étude. Exemple : "BAC +2, BAC +3, BAC +4"')

    ->setLocation('post_type', '==', PostTypes::POST_TYPE_JOB)
;

return $fields;
