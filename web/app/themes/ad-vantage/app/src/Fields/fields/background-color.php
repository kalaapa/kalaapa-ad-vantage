<?php

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Theme\Colors;

$bg_color = new FieldsBuilder('background');
$bg_color
    ->addSelect('background_color', [
        'ui'            => 1,
        'class'         => 'js-color-select',
        'allow_null'    => 0,
    ])
        ->setConfig('label', 'Couleur de fond')
        ->setRequired()
        ->addChoices(Colors::get_colors('label'))
;

return $bg_color;
