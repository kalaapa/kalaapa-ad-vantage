<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$content = new FieldsBuilder('content');
$content
    ->addText('title')
        ->setConfig('label', 'Titre')
        ->setRequired()
    ->addWysiwyg('content')
        ->setConfig('label', 'Contenu')
        ->setConfig('media_upload', false)
;

return $content;
