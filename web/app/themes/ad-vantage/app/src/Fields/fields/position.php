<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$align = new FieldsBuilder('position');
$align
    ->addRadio('content_position')
        ->setConfig('label', 'Position du texte')
        ->setDefaultValue('left')
        ->addChoices([
            'left' => 'Gauche',
            'right' => 'Droite',
        ])
;

return $align;
