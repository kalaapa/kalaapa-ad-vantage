<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\PostTypes;

$fields = new FieldsBuilder('home_meta', [
    'title'                 => 'Image de fond',
    'position'              => 'side',
    'instruction_placement' => 'field',
    'menu_order'            => 10,
]);

$fields
    ->addImage('home_background_image')
        ->setConfig('label', 'Image de fond')
    ->addImage('home_image_mobile')
        ->setConfig('label', 'Image mobile')
    ->setLocation('page_type', '==', 'front_page')
;

return $fields;
