<?php

namespace App;

use StoutLogic\AcfBuilder\FieldsBuilder;
use App\Admin\Options;

$fields = new FieldsBuilder('option_job_list', [
    'title' => 'Offres d’emploi',
    'instruction_placement' => 'field',
]);
$fields
    ->addImage('job_cover')
        ->setConfig('label', 'Image de la page offre d’emploi')
    ->addWysiwyg('job_content')
        ->setConfig('label', 'Contenu de la page offre d’emploi')
    ->setLocation('options_page', '==', Options::SLUG_JOB)
;

return $fields;
