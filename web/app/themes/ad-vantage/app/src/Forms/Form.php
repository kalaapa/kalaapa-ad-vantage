<?php

namespace App\Forms;

use Timber;

abstract class Form {

    public $inputs;

    public $isValid = false;

    public $isSubmitted = false;

    public $errors = [];

    protected $form;

    protected $email_to;

    protected $error_messages = [
        'email'    => 'Cet email n’est pas valide',
        'required' => 'Ce champ est requis',
        'accept'   => 'Ce type de fichier n’est pas autorisé',
    ];

    public function form_inputs() {
        return $this->form->getIterator();
    }

    public function form_start() {
        return $this->form->getOpeningTag();
    }

    public function form_end() {
        return $this->form->getClosingTag();
    }

    public function form_label($name) {
        $input = $this->form->offsetGet($name);
        if(!$input) {
            return;
        }
        if( !$input->getAttribute('required') ) {
            // $input->label->innerHTML = $input->label->innerHTML . ' - <span>Optionnel</span>';
        }
        return $input->label;
    }

    public function form_input($name) {
        $input = $this->form->offsetGet($name);
        if(!$input) {
            return;
        }
        $input->setTemplate('{{ input }}');
        return $input;
    }

    public function form_error($name) {
        if($this->form->offsetGet($name)) {
            return $this->form->offsetGet($name)->getError();
        }
    }

    protected function send_email($email_to, $title, $data) {
        $context = Timber::get_context();
        $context['data'] = $data;
        $message = Timber::fetch( 'emails/contact.html.twig', $context );
        $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

        $context['data']['sent_date'] = current_time('timestamp');

        return wp_mail(
            $email_to,
            wp_specialchars_decode( sprintf('[%s] - %s', $site_name, $title) ),
            $message,
            [ 'Content-Type: text/html; charset=UTF-8' ]
        );
    }

    abstract protected function handle();
    abstract protected function form();

}
