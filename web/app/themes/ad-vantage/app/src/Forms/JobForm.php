<?php

namespace App\Forms;

use FormManager\Factory as F;
use Timber;
use Timber\Post;
use Symfony\Component\Validator\Constraints;
use WP_Post;
use App\PostTypes;

class JobForm extends Form {

    const ACTION = 'apply';

    private $mime_types = [
        'application/msword',
        'application/pdf',
        'application/x-pdf',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.oasis.opendocument.text',
        'application/rtf',
    ];

    private $extensions = [
        '.doc',
        '.pdf',
        '.docx',
        '.odt',
        '.rtf',
    ];

    public function __construct()
    {
        $this->email_to = sanitize_email(get_option('options_job_email_to', get_option('admin_email')));
        $this->form = $this->form();
        $this->handle();
    }

    protected function handle() {
        if( !isset($_POST['submit']) ) {
            return;
        }

        $this->form->loadFromArrays([], $_POST, $_FILES);
        $this->isSubmitted = true;
        if( !$this->form->isValid() ) {
            $this->form->setAttribute('class', 'was-validated needs-validation');
            $this->errors = [
                'Veuillez corriger les champs signalés.'
            ];
            return;
        }

        $data = $this->form->getValue();
        $offer = new Post($data['offer_id']);

        $context = Timber::get_context();

        $site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
        $title = sprintf( '[%s] Candidature - %s - Ref : %s', $site_name, $offer->title, $offer->identifier );

        $context['data'] = $data;
        $context['data']['offer'] = $offer;

        $message = Timber::fetch( 'emails/apply.html.twig', $context );

        $cv = $data['cv'];
        $letter = $data['cover_letter'];

        $attach_file = function($phpmailer) use ($cv, $letter) {
            try {
                if( !empty($cv['size']) ) {
                    $phpmailer->addAttachment($cv['tmp_name'], $cv['name']);
                }
                if( !empty($letter['size']) ) {
                    $phpmailer->addAttachment($letter['tmp_name'], $letter['name']);
                }
            } catch ( \phpmailerException $e ) {

            }
        };

        add_action('phpmailer_init', $attach_file);

        $email = wp_mail(
            $this->email_to,
            wp_specialchars_decode( $title ),
            $message,
            [ 'Content-Type: text/html; charset=UTF-8' ]
        );

        remove_action('phpmailer_init', $attach_file);

        if( $email ) {
            // Reset form
            $this->form = $this->form();
            $this->isValid = true;
            $this->isSubmitted = true;
        } else {
            $this->form->setAttribute('class', 'was-validated');
            $this->errors = [
                'Une erreur s’est produite lors de l’envoi de votre message. Veuillez essayer à nouveau plus tard.'
            ];
        }
    }

    protected function form() {

        $post = new Post();
        $cities = $post->terms(PostTypes::TAXONOMY_CITY);
        $form_id = get_the_ID();
        $nonce = sprintf('%s_%s', $this::ACTION, $form_id);

        $fields = [
            'offer_id'           => F::hidden()
                ->setAttribute('required', 'required')
                ->setValue($form_id)
                ->addConstraint(new Constraints\Callback(function($value, $context) {
                    $post = WP_Post::get_instance( $value );
                    if( empty($post) ) {
                        $context
                            ->buildViolation('L’offre n’existe pas')
                            ->addViolation();
                        return;
                    }
                    if( $post->post_type !== PostTypes::POST_TYPE_JOB ) {
                        $context
                            ->buildViolation('L’offre n’est pas valide')
                            ->addViolation();
                        return;
                    }
                }))
            ,
            // 'nonce' => F::hidden()
            //     ->setValue(wp_create_nonce( $nonce ))
            //     ->addConstraint(new Constraints\Callback(function($value, $context) use ($nonce) {
            //         if( wp_verify_nonce( $value, $nonce ) ) {
            //             return;
            //         }
            //         $context->addViolation('Nonce error');
            //     }))
            // ,
            'lastname'           => F::text('Nom', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'firstname'          => F::text('Prénom', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'email'              => F::email('Email', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'cv'        => F::file('CV', [
                'required' => 'required',
                'accept' => implode(',', array_merge($this->extensions, $this->mime_types)),
            ])
                ->setLabel('CV', ['class' => 'custom-file-label'])
                // ->setErrorMessages($this->error_messages)
                ->addConstraint(new Constraints\Callback(function($object, $context) {
                    if( isset($object['error']) && $object['error'] === 4 ) {
                        $context->buildViolation('Ce champ est requis')
                        ->addViolation();
                    }
                    if( isset($object['size']) && $object['size'] === 0 ) {
                        $context->buildViolation('Ce champ est requis')
                        ->addViolation();
                    }
                }))
            ,
            'cover_letter'        => F::file('Lettre de motivation', [
                'accept' => implode(',', array_merge($this->extensions, $this->mime_types)),
            ])
                ->setLabel('Lettre de motivation', ['class' => 'custom-file-label'])
            ,
        ];

        if( get_option('options_recaptcha_site_secret') && get_option('recaptcha_site_key') ) {
            $fields['recaptcha_response'] = F::hidden()
                ->setAttribute('id', 'recaptcha_response')
                ->setAttribute('required', 'required')
                ->addConstraint(new Constraints\Callback(function($value, $context) {

                    $secret = get_option('options_recaptcha_site_secret');
                    if( empty($secret) ) {
                        return;
                    }

                    // check if enabled
                    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
                    $response = $recaptcha
                        ->setExpectedAction($this::ACTION)
                        ->verify($value);
                    if ($response->isSuccess()) {
                        return;
                    }
                    foreach($response->getErrorCodes() as $error) {
                        $context
                            ->buildViolation($error)
                            ->addViolation();
                    }
                }))
            ;
        }

        if( count($cities) > 1 ) {
            $cities_names = wp_list_pluck($cities, 'name');
            // Add city field
            $fields['city'] = F::select(
                'Ville',
                array_combine($cities_names, $cities_names),
                [
                    'required' => 'required'
                ]
            )
            ->addConstraint(new Constraints\Choice([
                'choices' => $cities_names,
                'message' => 'La ville n‘est pas valide',
            ]));
        }

        $form = F::form($fields);

        $form->setAttributes([
            'action'     => get_permalink() . '#apply',
            'method'     => 'post',
            'enctype'    => 'multipart/form-data',
            'novalidate' => 'novalidate',
            'class'      => 'needs-validation',
        ]);

        $this->isValid = false;
        $this->isSubmitted = false;

        return $form;
    }

}
