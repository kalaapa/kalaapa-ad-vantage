<?php

namespace App\Forms;

use FormManager\Factory as F;
use Timber;
use Timber\Post;
use Symfony\Component\Validator\Constraints;
use WP_Post;
use App\PostTypes;

class ContactForm extends Form {

    const ACTION = 'contact';

    public function __construct()
    {
        $this->email_to = sanitize_email(get_option('options_contact_email_to', get_option('admin_email')));
        $this->form = $this->form();
        $this->handle();
    }

    protected function handle() {
        if( !isset($_POST['submit']) ) {
            return;
        }

        $this->form->loadFromArrays([], $_POST);
        $this->isSubmitted = true;
        if( !$this->form->isValid() ) {
            $this->form->setAttribute('class', 'was-validated needs-validation');
            $this->errors = [
                'Veuillez corriger les champs signalés.'
            ];
            return;
        }

        $data = $this->form->getValue();

        if( $this->send_email($this->email_to, 'Formulaire de contact', $data) ) {
            // Reset form
            $this->form = $this->form();
            $this->isValid = true;
            $this->isSubmitted = true;
        } else {
            $this->form->setAttribute('class', 'was-validated');
            $this->errors = [
                'Une erreur s’est produite lors de l’envoi de votre message. Veuillez essayer à nouveau plus tard.'
            ];
        }
    }

    protected function form() {

        $form_id = get_the_ID();
        $nonce = sprintf('%s_%s', $this::ACTION, $form_id);

        $fields = [
            // 'nonce' => F::hidden()
            //     ->setValue(wp_create_nonce( $nonce ))
            //     ->addConstraint(new Constraints\Callback(function($value, $context) use ($nonce) {
            //         if( wp_verify_nonce( $value, $nonce ) ) {
            //             return;
            //         }
            //         $context->addViolation('Nonce error');
            //     }))
            // ,
            'lastname'           => F::text('Nom', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'firstname'          => F::text('Prénom', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'email'              => F::email('Email', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'object'              => F::text('Objet', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),
            'message'              => F::textarea('Message', [
                'required' => 'required'
            ])->setErrorMessages($this->error_messages),

        ];

        if( get_option('options_recaptcha_site_secret') && get_option('recaptcha_site_key') ) {
            $fields['recaptcha_response'] = F::hidden()
                ->setAttribute('id', 'recaptcha_response')
                ->setAttribute('required', 'required')
                ->addConstraint(new Constraints\Callback(function($value, $context) {

                    $secret = get_option('options_recaptcha_site_secret');
                    if( empty($secret) ) {
                        return;
                    }

                    // check if enabled
                    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
                    $response = $recaptcha
                        ->setExpectedAction($this::ACTION)
                        ->verify($value);
                    if ($response->isSuccess()) {
                        return;
                    }
                    foreach($response->getErrorCodes() as $error) {
                        $context
                            ->buildViolation($error)
                            ->addViolation();
                    }
                }))
            ;
        }

        $form = F::form($fields);

        $form->setAttributes([
            'action'     => get_permalink(),
            'method'     => 'post',
            'novalidate' => 'novalidate',
            'class'      => 'needs-validation',
        ]);

        $this->isValid = false;
        $this->isSubmitted = false;

        return $form;
    }

}
