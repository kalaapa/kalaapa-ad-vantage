<?php

namespace App\Acf;

use Timber;
use Timber\Image;

class Embed {

    public function __construct() {
        add_filter('embed_oembed_html', [$this, 'wrap_video_embed'], 10, 4);
        add_filter('oembed_dataparse', [$this, 'save_oembed_data'], 10, 3);
        $this->cache_acf_oembed();

        add_action( 'acf/init', function () {
            $field_type = acf_get_field_type('image');
            remove_filter( 'acf/format_value/type=image', [ $field_type, 'format_value' ] );
            $field_type = acf_get_field_type('gallery');
            remove_filter( 'acf/format_value/type=gallery', [ $field_type, 'format_value' ] );
        });
        add_filter( 'acf/format_value/type=image', [$this, 'format_acf_image'], 10, 3);
        add_filter( 'acf/format_value/type=gallery', [$this, 'format_acf_gallery'], 10, 3);
    }


    public function format_acf_gallery( $value, $post_id, $field ) {
        if( empty($value) ) {
            return false;
        }

        return array_map(function($attachment_id) {
            return new Image($attachment_id);
        }, $value);
    }

    public function format_acf_image($value, $post_id, $field) {
		if( empty($value) ) {
            return false;
        }

		if( !is_numeric($value) ) {
            return false;
        }

        $value = intval($value);

        return new Image($value);
    }

    /**
     * Wrap video embeds
     *
     * @param string $html
     * @param string $url
     * @param array $attr
     * @param integer $post_id
     * @return string
     */
    public function wrap_video_embed( $html, $url, $attr, $post_id ) {
        $embed_data = get_post_meta( $post_id, sprintf('_embed_data_%s', md5($url)), true );

        if(!isset($embed_data['type'])) {
            return $html;
        }
        if($embed_data['type'] !== 'video') {
            return $html;
        }

        $html = str_replace('<iframe', sprintf('<iframe data-consent="%s" class="lazyload embed-responsive-item"', strtolower($embed_data['provider_name'])), $html);
        if(!is_admin()) {
            $html = str_replace('src="', 'data-src="', $html);
        }

        return [
            'iframe' => $html,
            'data' => $embed_data,
            'url' => $url,
        ];

        // $context = [
        //     'iframe' => $html,
        //     'embed_data' => $embed_data,
        // ];

        // return Timber::compile('consent/embed.html.twig', $context);
    }

    /**
     * Save oEmbed data
     *
     * @param string $html
     * @param array $data
     * @param string $url
     * @return string
     */
    public function save_oembed_data($html, $data, $url) {
        $post = get_post();
        if(empty($post->ID)) {
            return $html;
        }

        $key = md5($url);
        update_post_meta($post->ID, '_embed_data_' . $key, (array) $data);

        return $html;
    }

    /**
     * Caches oEmbed ACF field
     *
     * @see https://support.advancedcustomfields.com/forums/topic/oembed-cache/
     */
    public function cache_acf_oembed() {

        /** Disables acf_field_oembed::format_value() */
        add_action( 'acf/init', function () {
            $field_type = acf_get_field_type('oembed');
            remove_filter( 'acf/format_value/type=oembed', [ $field_type, 'format_value' ] );
        }, 1 );

        /** Fetch the cached oEmbed HTML; Replaces the original method */
        add_filter( 'acf/format_value/type=oembed', function ( $value, $post_id, $field ) {
            if ( ! empty( $value ) ) {
                $value = $this->acf_oembed_get( $value, $post_id, $field );
            }

            return $value;
        }, 10, 3 );

        /** Cache the oEmbed HTML */
        add_filter( 'acf/update_value/type=oembed', function ( $value, $post_id, $field ) {
            if ( ! empty( $value ) ) {
                // Warm the cache
                $this->acf_oembed_get( $value, $post_id, $field );
            }

            return $value;
        }, 10, 3 );
    }

    /**
     * Attempts to fetch the embed HTML for a provided URL using oEmbed.
     *
     * Checks for a cached result (stored as custom post or in the post meta).
     *
     * @see  \WP_Embed::shortcode()
     *
     * @param  mixed   $value   The URL to cache.
     * @param  integer $post_id The post ID to save against.
     * @param  array   $field   The field structure.
     * @return string|null The embed HTML on success, otherwise the original URL.
     */
    private function acf_oembed_get( $value, $post_id, $field )
    {
        if ( empty( $value ) ) {
            return $value;
        }

        global $wp_embed;

        $attr = [
            'width'  => $field['width'],
            'height' => $field['height'],
        ];

        $html = $wp_embed->shortcode([], $value);

        if ( $html ) {
            return $html;
        }

        return $value;
    }

}

