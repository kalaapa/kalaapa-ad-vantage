<?php

namespace App\Acf;

class GoogleMap {

    private $api_front_key;
    private $api_back_key;

    public function __construct() {
        if( !is_admin() ) {
            return;
        }
        $this->api_front_key = get_option('options_job_front_google_api_key');
        $this->api_back_key = get_option('options_job_back_google_api_key');
        add_filter('acf/fields/google_map/api', [$this, 'set_google_map_api_key']);
        add_action('acf/update_value/type=google_map', [$this, 'save_map_data'], 10, 3);
        add_action('acf/render_field/type=google_map', [$this, 'show_map_data'], 10, 1 );
    }

    public function show_map_data( $field ) {
        $postal_address = [];
        if( empty($field['value']['postal_address']) ) {
            echo '<p style=""><strong>⚠️ Aucune donnée de localisation n’est enregistrée</strong></p>';
        } else {
            $postal_address = $field['value']['postal_address'];
        }
        ?>
        <p>
            <label style="display:inline-block;width:120px;">Adresse :</label>
            <input value="<?php echo $postal_address['street_address'] ?? null ?>" readonly class="regular-text" />
        </p>
        <p>
        <label style="display:inline-block;width:120px;">Code postal :</label>
            <input value="<?php echo $postal_address['postal_code'] ?? null ?>" readonly class="regular-text" />
        </p>
        <p>
        <label style="display:inline-block;width:120px;">Ville :</label>
            <input value="<?php echo $postal_address['city'] ?? null ?>" readonly class="regular-text" />
        </p>
        <p>
        <label style="display:inline-block;width:120px;">Région :</label>
            <input value="<?php echo $postal_address['region'] ?? null ?>" readonly class="regular-text" />
        </p>
        <p>
        <label style="display:inline-block;width:120px;">Pays :</label>
            <input value="<?php echo $postal_address['country'] ?? null ?>" readonly class="regular-text" />
        </p>
        <?php
    }

    public function save_map_data( $value, $post_id, $field ) {
        if(empty($value['address']) || empty($this->api_back_key)) {
            return $value;
        }

        $results = false;
        try {
            $client = new \Http\Adapter\Guzzle6\Client();
            $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($client, null, $this->api_back_key);
            $geocoder = new \Geocoder\StatefulGeocoder($provider, get_locale());
            $results = $geocoder->geocodeQuery(\Geocoder\Query\GeocodeQuery::create($value['address']));
        } catch(\Exception $e) {

        }

        if(empty($results)) {
            return $value;
        }
        $location = $results->first();
        $formatter = new \Geocoder\Formatter\StringFormatter();

        $postal_address = [
            'street_address' => $formatter->format($location, '%n %S'),
            'postal_code'    => $location->getPostalCode(),
            'city'           => $location->getLocality(),
            'region'         => $formatter->format($location, '%A1'),
            'country'        => $formatter->format($location, '%C'),
        ];

        $value['postal_address'] = array_filter($postal_address);

        return $value;
    }

    public function set_google_map_api_key( $api ) {
        $api['key'] = $this->api_front_key;
        return $api;
    }

}
