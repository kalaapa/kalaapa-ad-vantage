<?php

namespace App\Schema;

use App\PostTypes;
use Spatie\SchemaOrg\Schema;
use Timber\Post;

class Job {

    public function __construct() {
        add_action('wp_head', [$this, 'add_schema']);
    }

    public function add_schema() {

        // Only include on Jobs
        if( !is_singular(PostTypes::POST_TYPE_JOB) ) {
            return;
        }

        $post = new Post();
        $expiration_date = new \DateTime($post->validThrough);
        $now = new \DateTime(current_time('mysql'));

        // Remove Schema if offer has expired
        if( $expiration_date <= $now ) {
            return;
        }

        // Job location is required
        $locations = $post->meta('locations');

        if(empty($locations)) {
            return;
        }
        if(empty($locations[0]['jobLocation']['postal_address'])) {
            return;
        }

        $skills = $post->terms(PostTypes::TAXONOMY_SKILL);

        $job_locations = [];
        foreach($locations as $location) {
            $postal_address = $location['jobLocation']['postal_address'];
            $job_locations[] = Schema::Place()
                ->address(
                    Schema::PostalAddress()
                        ->if(!empty($postal_address['street_address']), function($schema) use ($postal_address) {
                            $schema->streetAddress($postal_address['street_address']);
                        })
                        ->if(!empty($postal_address['postal_code']), function($schema) use ($postal_address) {
                            $schema->postalCode($postal_address['postal_code']);
                        })
                        ->if(!empty($postal_address['city']), function($schema) use ($postal_address) {
                            $schema->addressLocality($postal_address['city']);
                        })
                        ->if(!empty($postal_address['region']), function($schema) use ($postal_address) {
                            $schema->addressRegion($postal_address['region']);
                        })
                        ->if(!empty($postal_address['country']), function($schema) use ($postal_address) {
                            $schema->addressCountry($postal_address['country']);
                        })
                )
            ;
        }

        echo Schema::JobPosting()
            ->title($post->title())
            ->description($post->content())
            ->datePosted(new \DateTime($post->post_date))
            ->validThrough($expiration_date)
            ->if(!empty($post->employmentType), function ($schema) use($post) {
                $schema->employmentType($post->employmentType);
            })
            ->if(!empty($post->identifier), function ($schema) use($post) {
                $schema->identifier(
                    Schema::PropertyValue()
                        ->name($post->hiringOrganization)
                        ->value($post->identifier)
                );
            })
            ->hiringOrganization(Schema::Organization()
                ->name($post->hiringOrganization)
            )
            ->jobLocation(count($job_locations) === 1 ? $job_locations[0] : $job_locations)
            ->if(!empty($post->qualifications), function ($schema) use($post) {
                $schema->qualifications($post->qualifications);
            })
            ->if(!empty($post->educationRequirements), function ($schema) use($post) {
                $schema->educationRequirements($post->educationRequirements);
            })
            ->if(!empty($skills), function ($schema) use($skills) {
                $schema->skills(wp_list_pluck($skills, 'name'));
            })
            ->toScript();
    }

}
