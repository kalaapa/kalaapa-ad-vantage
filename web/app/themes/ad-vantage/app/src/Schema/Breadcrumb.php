<?php

namespace App\Schema;

use Spatie\SchemaOrg\Schema;

class Breadcrumb {

    public function __construct() {
        add_action('wp_head', [$this, 'add_schema']);
        add_filter('option_autodescription-site-settings', [$this, 'disable_tsf_breadcrumb']);
    }

    public function disable_tsf_breadcrumb($option) {
        $option['ld_json_breadcrumbs'] = 0;
        return $option;
    }

    public function add_schema() {
        $items = \App\container()->get('app.breadcrumb');

        // Don't include JSON-LD if less than 2 items
        if( count($items) <= 1 ) {
            return;
        }

        array_walk($items, function(&$item, $k) {
            $item = Schema::listItem()
                ->position($k + 1)
                ->name($item['title'])
                ->item($item['link']);
        });

        echo Schema::BreadcrumbList()
            ->itemListElement($items)
            ->toScript();
    }

}
