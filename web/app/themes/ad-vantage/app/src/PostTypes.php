<?php

namespace App;

use App\Theme\Colors;

class PostTypes {

    const POST_TYPE_JOB   = 'job';

    const TAXONOMY_CITY   = 'city';
    const TAXONOMY_SKILL   = 'skill';

    public function __construct() {
        add_action('init', [$this, 'register_post_types']);
        add_action('init', [$this, 'register_taxonomies']);

        // Order relationship by date
        // Remove current post from relations
        // Only show published posts
        add_filter('acf/fields/relationship/query', function($args, $field, $post_id) {
            $args['post__not_in'] = [ $post_id ];
            $args['post_status'] = 'publish';
            $args['orderby'] = 'date';
            $args['order'] = 'desc';
            return $args;
        }, 10, 3);

    }

    /**
     * Register taxonomies
     */
    public function register_taxonomies() {

        $labels = [
            'name'                       => 'Villes',
            'singular_name'              => 'Ville',
            'menu_name'                  => 'Villes',
            'all_items'                  => 'Toutes les villes',
            'parent_item'                => 'Ville parente',
            'parent_item_colon'          => 'Ville parente :',
            'new_item_name'              => 'Nouvelle ville',
            'add_new_item'               => 'Ajouter une nouvelle ville',
            'edit_item'                  => 'Modifier la ville',
            'update_item'                => 'Mettre à jour la ville',
            'view_item'                  => 'Voir la ville',
            'separate_items_with_commas' => 'Séparer les villes par des virgules',
            'add_or_remove_items'        => 'Ajouter ou supprimer une ville',
            'choose_from_most_used'      => __( 'Choose from the most used', 'adv' ),
            'popular_items'              => 'Villes populaires',
            'search_items'               => 'Rechercher une ville',
            'not_found'                  => 'Aucune ville trouvée',
            'no_terms'                   => 'Aucune ville',
            // 'items_list'                 => __( 'Age ranges list', 'adv' ),
            // 'items_list_navigation'      => __( 'Age ranges list navigation', 'adv' ),
            'back_to_items'              => '&larr; Retour aux villes',
        ];

        register_extended_taxonomy( $this::TAXONOMY_CITY, [$this::POST_TYPE_JOB], [
            'public'            => true,
            'meta_box'          => null,
            'show_in_nav_menus' => false,
            'rewrite' => [
                'slug' => 'ville',
            ],
            'labels'       => $labels,

            'admin_cols' => [

            ]
        ]);

        $labels = [
            'name'                       => 'Compétences',
            'singular_name'              => 'Compétence',
            'menu_name'                  => 'Compétences',
            'all_items'                  => 'Toutes les compétences',
            'parent_item'                => 'Compétence parente',
            'parent_item_colon'          => 'Compétence parente :',
            'new_item_name'              => 'Nouvelle compétence',
            'add_new_item'               => 'Ajouter une nouvelle compétence',
            'edit_item'                  => 'Modifier la compétence',
            'update_item'                => 'Mettre à jour la compétence',
            'view_item'                  => 'Voir la compétence',
            'separate_items_with_commas' => 'Séparer les compétences par des virgules',
            'add_or_remove_items'        => 'Ajouter ou supprimer une compétence',
            'choose_from_most_used'      => __( 'Choose from the most used', 'adv' ),
            'popular_items'              => 'Compétences populaires',
            'search_items'               => 'Rechercher une compétence',
            'not_found'                  => 'Aucune compétence trouvée',
            'no_terms'                   => 'Aucune compétence',
            // 'items_list'                 => __( 'Age ranges list', 'adv' ),
            // 'items_list_navigation'      => __( 'Age ranges list navigation', 'adv' ),
            'back_to_items'              => '&larr; Retour aux compétences',
        ];

        register_extended_taxonomy( $this::TAXONOMY_SKILL, [$this::POST_TYPE_JOB], [
            'public'            => true,
            'meta_box'          => 'simple',
            'show_in_nav_menus' => false,
            'hierarchical'      => false,
            'rewrite' => [
                'slug' => 'comptence',
            ],
            'labels'       => $labels,

            'admin_cols' => [

            ]
        ]);

    }

    /**
     * Register post types
     */
    public function register_post_types() {
        $this->register_job();
    }

    /**
     * Register job
     */
    public function register_job() {

        $labels = [
            'name'                  => 'Offres d’emploi',
            'singular_name'         => 'Offre d’emploi',
            'menu_name'             => 'Offres d’emploi',
            'name_admin_bar'        => 'Offre d’emploi',
            'archives'              => 'Archives des offres d’emploi',
            'attributes'            => 'Attributs des offres d’emploi',
            'parent_item_colon'     => 'Offre d’emploi parente:',
            'all_items'             => 'Toutes les offres d’emploi',
            'add_new_item'          => 'Ajout une nouvelle offre d’emploi',
            'add_new'               => 'Ajouter',
            'new_item'              => 'Nouvelle offre d’emploi',
            'edit_item'             => 'Modifier l’offre d’emploi',
            'update_item'           => 'Mettre à jour l’offre d’emploi',
            'view_item'             => 'Voir l’offre d’emploi',
            'view_items'            => 'Voir les offres d’emploi',
            'search_items'          => 'Rechercher une offre d’emploi',
            'not_found'             => 'Aucune offre d’emploi trouvée',
            'not_found_in_trash'    => 'Aucune offre d’emploi trouvée dans la corbeille',
            // 'featured_image'        => 'Image de l’offre d’emploi',
            // 'set_featured_image'    => 'Set product image',
            // 'remove_featured_image' => 'Remove product image',
            // 'use_featured_image'    => 'Use as product image',
            // 'insert_into_item'      => 'Insert into product',
            // 'uploaded_to_this_item' => 'Uploaded to this product',
            'items_list'            => 'Liste des offres d’emploi',
            'items_list_navigation' => 'Navigation de la liste des offres d’emploi',
            'filter_items_list'     => 'Filtrer la liste des offre d’emploi',
        ];

        register_extended_post_type( $this::POST_TYPE_JOB, [
            'public'            => true,
            // 'show_in_nav_menus' => false,
            'menu_icon'         => 'dashicons-tag',
            'supports'          => ['title', 'editor'],
            'labels'            => $labels,
            'hierarchical'      => false,
            'has_archive'       => true,
            'rewrite' => [
                'slug' => 'offres-emploi',
                'feeds' => false,
                'pages' => false,
            ],

            'enter_title_here' => 'Titre de l’offre',

            'admin_filters' => [
                'city' => [
                    'taxonomy' => $this::TAXONOMY_CITY,
                ],
            ],

            'register_meta_box_cb' => [$this, 'add_structured_data_meta_box'],

            'admin_cols' => [
                'title' => [
                    'title' => 'Titre',
                    'default'  => 'ASC',
                ],
                'posted_date' => [
                    'title' => 'Date de publication',
                    'post_field' => 'post_date',
                    'default'  => 'ASC',
                ],
                'valid_though' => [
                    'title' => 'Date d’expiration',
                    'meta_key' => 'validThrough',
                    'date_format' => 'd M Y',
                ],
                'city' => [
                    'taxonomy' => $this::TAXONOMY_CITY,
                ],
                'skill' => [
                    'taxonomy' => $this::TAXONOMY_SKILL,
                ],
            ],

        ]);

    }

    public function add_structured_data_meta_box() {
        add_meta_box(
            'job-structured-data',
            'Job Posting',
            [$this, 'structured_data_meta_box'],
            [PostTypes::POST_TYPE_JOB],
            'side',
            'high'
        );
    }

    public function structured_data_meta_box($post) {
        if( empty($post->post_status) ) {
            return;
        }
        if( $post->post_status !== 'publish' ) {
            return;
        }
        ?>
        <div style="padding:5px;">
            <a href="https://search.google.com/structured-data/testing-tool#url=<?php the_permalink() ?>" target="_blank" rel="noopener" class="button-secondary">Tester les données structurées</a>
        </div>
        <?php
    }


}
