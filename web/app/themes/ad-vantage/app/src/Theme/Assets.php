<?php

namespace App\Theme;

class Assets {

    public function __construct() {

        if ( is_admin() ) {
            return;
        }

        add_action( 'wp_enqueue_scripts', [ $this, 'register_assets' ], 1 );
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );
        add_filter( 'script_loader_tag', [ $this, 'add_defer' ], 10, 2 );
        add_filter( 'script_loader_tag', [ $this, 'remove_type' ] );
        add_filter( 'style_loader_tag', [ $this, 'remove_type' ] );
    }

    public function remove_type( $tag ) {
        $tag = str_replace( " type='text/javascript'", '', $tag );
        $tag = str_replace( " type='text/css'", '', $tag );
        return $tag;
    }

    public function add_defer( $tag, $handle ) {
        if ( $handle !== 'app' ) {
            return $tag;
        }
        return str_replace( ' src', ' defer async src', $tag );
    }

    /**
     * Enqueue scripts
     */
    public function enqueue_scripts() {
        wp_enqueue_style( 'app' );
        wp_enqueue_script( 'app' );
    }

    /**
     * Enqueue styles
     */
    public function enqueue_styles() {
    }

    /**
     * Register styles
     */
    private function register_styles() {
        wp_register_style(
            'app',
            \App\container()->get('assets.packages')->getUrl(sprintf('css/app%s.css', is_rtl() ? '-rtl' : '')),
            false,
            null
        );
    }

    /**
     * Register scripts
     */
    private function register_scripts() {

        if ( is_admin() ) {
            return;
        }

        wp_register_script(
            'app',
            \App\container()->get('assets.packages')->getUrl('js/app.js'),
            false,
            null,
            true
        );

        $js_args = apply_filters('app/js/args', []);
        if($js_args) {
            wp_localize_script('app', 'app', $js_args);
        }
    }

    /**
     * Register styles & scripts
     */
    public function register_assets() {
        $this->register_scripts();
        $this->register_styles();
    }

}
