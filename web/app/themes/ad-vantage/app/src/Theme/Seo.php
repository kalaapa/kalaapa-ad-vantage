<?php

namespace App\Theme;

use App\PostTypes;

class Seo {

    public function __construct() {

        add_filter('the_seo_framework_indicator', '__return_false');

        // Sitemap
        add_filter( 'the_seo_framework_sitemap_pages_query_args', [$this, 'add_languages_to_sitemap'], 10, 2);
        add_filter( 'the_seo_framework_sitemap_posts_query_args', [$this, 'add_languages_to_sitemap'], 10, 2);
        add_filter( 'the_seo_framework_sitemap_cpt_query_args', [$this, 'add_languages_to_sitemap'], 10, 2);
        add_filter( 'the_seo_framework_sitemap_additional_urls', [$this, 'add_cpt_archives_to_sitemap']);

        add_filter( 'robots_txt', [$this, 'fix_robots_txt'], 10, 2 );
    }

    /**
     * Fix robots.txt
     *
     * @param string $robots
     * @param integer $public
     * @return string
     */
    public function fix_robots_txt($robots, $public) {

		if ( '0' === (string) $public ) {
            return $robots;
        }

        $site_url = \wp_parse_url( \site_url() );
        $site_path = ( ! empty( $site_url['path'] ) ) ? \esc_attr( $site_url['path'] ) : '';
        $robots = "User-agent: *\r\n";
        $robots .= "Disallow: $site_path/wp-admin/\r\n";
        $robots .= "Allow: $site_path/wp-admin/admin-ajax.php\r\n";

        $tsf = the_seo_framework();
        if ( $tsf->can_do_sitemap_robots( true ) ) {
            $robots .= "\r\nSitemap: " . \esc_url( \App\container()->get('paths.home_root_url') ) . "sitemap.xml\r\n";
        }
        return $robots;
    }

    /**
     * Add languages to sitemap query
     *
     * @param array $args
     * @param array $defaults
     * @return array
     */
    public function add_languages_to_sitemap($args, $defaults) {
        if( !function_exists('pll_languages_list') ) {
            return $args;
        }
        $languages = pll_languages_list();
        if(empty($languages)) {
            return $args;
        }
        $args['lang'] = implode(',', $languages);
        return $args;
    }

    /**
     * Add custom post type archive to sitemap
     *
     * @param array $custom_urls
     * @return array
     */
    public function add_cpt_archives_to_sitemap($custom_urls) {

		$post_types = get_post_types( [ 'public' => true ] );
        $post_types = array_filter( $post_types, function($post_type) {
            return is_post_type_viewable($post_type) && !in_array($post_type, get_post_types(['_builtin' => true]));
        });

        if(empty($post_types)) {
            return $custom_urls;
        }

        if( !function_exists('PLL') ) {
            foreach($post_types as $post_type) {
                $url = get_post_type_archive_link($post_type);
                $custom_urls[$url] = [
                    'priority' => '0.8',
                    'lastmod' => $this->get_last_mod_in_post_type($post_type),
                ];
            }
            return $custom_urls;
        }

        $pll = PLL();
        $languages = $pll->model->get_languages_list();
        if(empty($languages)) {
            return $custom_urls;
        }

        foreach($languages as $language) {
            foreach($post_types as $post_type) {
                $url = $pll->links_model->switch_language_in_link(get_post_type_archive_link($post_type), $language);
                $url = $pll->translate_slugs->slugs_model->translate_slug($url, $language, 'archive_' . $post_type);
                if(!$url) {
                    continue;
                }
                $custom_urls[$url] = [
                    'lastmod' => $this->get_last_mod_in_post_type($post_type, $language),
                    'priority' => '0.8',
                ];
            }
        }

        return $custom_urls;
    }

    /**
     * Add terms to sitemap
     *
     * @param array $custom_urls
     * @return array
     */
    public function add_terms_to_sitemap($custom_urls) {

        $taxonomies = get_taxonomies( ['public' => true], 'objects' );
		if ( empty( $taxonomies ) ) {
			return $custom_urls;
        }

        $tsf = the_seo_framework();
        if( function_exists('pll_get_term_language') ) {
            $pll = PLL();
        }
        foreach($taxonomies as $taxonomy) {
            $terms = get_terms($taxonomy->name, [
                'hide_empty' => true,
                'lang' => 'irak,fr,en',
            ]);

            foreach ( $terms as $term ) {
                if ( !isset( $term->taxonomy ) ) {
                    continue;
                }
                // Get term URL
                $url = $tsf->create_canonical_url( [
                    'id'       => $term->term_id,
                    'taxonomy' => $term->taxonomy,
                ]);
                if(empty($url)) {
                    continue;
                }

                $language = false;
                if( function_exists('pll_get_term_language') ) {
                    $term_language = pll_get_term_language($term->term_id);
                    $language = $pll->model->get_language($term_language);
                }
                $custom_urls[$url] = [
                    'lastmod' => $this->get_last_mod_in_term($term, $language),
                    'priority' => '0.9',
                ];
            }
        }

        return $custom_urls;
    }

    /**
     * Get last modification date in term
     *
     * @param WP_Term $term
     * @return string
     */
    private function get_last_mod_in_term($term, $lang = false) {
        global $wpdb;

        if( function_exists('PLL') && $lang !== false ) {
            $pll = PLL();
            $sql = "
                SELECT MAX(p.post_modified_gmt) AS lastmod
                FROM	$wpdb->posts AS p
                {$pll->model->post->join_clause('p')}
                INNER JOIN $wpdb->term_relationships AS term_rel
                    ON		term_rel.object_id = p.ID
                INNER JOIN $wpdb->term_taxonomy AS term_tax
                    ON		term_tax.term_taxonomy_id = term_rel.term_taxonomy_id
                    AND		term_tax.taxonomy = %s
                    AND		term_tax.term_id = %d
                WHERE	p.post_status = 'publish'
                    AND		p.post_password = ''
                {$pll->model->post->where_clause( $lang )}
            ";
        } else {
            $sql = "
                SELECT MAX(p.post_modified_gmt) AS lastmod
                FROM	$wpdb->posts AS p
                INNER JOIN $wpdb->term_relationships AS term_rel
                    ON		term_rel.object_id = p.ID
                INNER JOIN $wpdb->term_taxonomy AS term_tax
                    ON		term_tax.term_taxonomy_id = term_rel.term_taxonomy_id
                    AND		term_tax.taxonomy = %s
                    AND		term_tax.term_id = %d
                WHERE	p.post_status = 'publish'
                    AND		p.post_password = ''
            ";
        }

        return $wpdb->get_var( $wpdb->prepare( $sql, $term->taxonomy, $term->term_id ) );
    }

    /**
     * Get last modification date in post type
     *
     * @param WP_Term $term
     * @return string
     */
    private function get_last_mod_in_post_type($post_type, $lang = false) {
        global $wpdb;
        if( function_exists('PLL') && $lang !== false ) {
            $pll = PLL();
            $sql = "
                SELECT MAX(post_modified_gmt) AS lastmod
                FROM	$wpdb->posts AS p
                {$pll->model->post->join_clause('p')}
                WHERE	p.post_status = 'publish'
                    AND		p.post_password = ''
                    AND		p.post_type = %s
                {$pll->model->post->where_clause( $lang )}
            ";
        } else {
            $sql = "
                SELECT MAX(p.post_modified_gmt) AS lastmod
                FROM	$wpdb->posts AS p
                WHERE	p.post_status = 'publish'
                    AND		p.post_password = ''
                    AND		p.post_type = %s
            ";
        }

        return $wpdb->get_var( $wpdb->prepare( $sql, $post_type ) );
    }
}
