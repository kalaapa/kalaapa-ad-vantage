<?php

namespace App\Theme;

class WhiteLabel {


    public function __construct() {
        add_action( 'login_head', [ $this, 'logo' ] );
        add_filter( 'login_headerurl', function ( $url ) {
            return home_url();
        });
        // add_filter( 'get_user_option_admin_color', function( $color_scheme ) {
        //     return 'theme';
        // });
        // add_action('admin_init', function() {
        //     wp_admin_css_color( 'theme', 'Custom',
        //         sprintf('%s/assets/css/admin.css', get_template_directory_uri()),
        //         [ '#46403c', '#59524c', '#c7a589', '#9ea476' ],
        //         [ 'base' => '#f3f2f1', 'focus' => '#fff', 'current' => '#fff' ]
        //     );
        // });
    }

    public function logo() {
        if( !($logo = $this->get_logo()) ) {
            return;
        }

        $extension = pathinfo($logo, PATHINFO_EXTENSION);

        if( $extension !== 'svg' ) {
            list($width, $height) = getimagesize($logo);
            list($width, $height) = $this->getSize($width, $height);
        } else {
            $xml = simplexml_load_file($logo);
            $attr = $xml->attributes();
            $viewbox = explode(' ', $attr->viewBox);
            $width = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
            $height = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
            list($width, $height) = $this->getSize($width, $height);
        }
        ?>
        <style>
        .login h1 a {
            width: <?php echo $width; ?>px;
            height: <?php echo $height; ?>px;
            background-size: contain;
            background-image: url(<?php printf('%s/assets/images/logo.%s', get_template_directory_uri(), $extension) ?>);
        }
        </style>
        <?php
    }

    private function get_logo() {
        $base_path = get_template_directory() . '/assets/images';
        foreach(['png', 'jpg', 'gif', 'svg'] as $ext) {
            $file = sprintf('%s/logo.%s', $base_path, $ext);
            if(!is_file($file)) {
                continue;
            }
            return $file;
        }
        return false;
    }

    private function getSize($width, $height, $max_width = 320, $max_height = 100) {

        $size['width'] = $max_width;
        $size['height'] = $max_height;

        if($width > $max_width || $height > $max_height) {
            if ( $height > $max_height ) {
                $size['height']  = $max_height;
                $size['width'] = floor(($width / $height) * $max_height);
            } else {
                $size['width']  = floor(($width / $height) * $max_height);
                $size['height'] = $max_height;
            }
        }
        return array_values($size);
    }
}
