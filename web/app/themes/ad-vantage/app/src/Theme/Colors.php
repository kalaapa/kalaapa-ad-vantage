<?php

namespace App\Theme;

class Colors {

    const COLORS = [
        'primary'        => [
            'label'    => 'Principale',
            'color'    => '#A1042D',
        ],
        'cyan'        => [
            'label'    => 'Picton Blue',
            'color'    => '#55C4F1',
        ],
        'teal'        => [
            'label'    => 'Niagara',
            'color'    => '#079A9B',
        ],
    ];

    public static function get_color_code($key) {
        $color = array_filter(self::COLORS, function($color) use ($key) {
            return $color === $key;
        }, ARRAY_FILTER_USE_KEY);

        return $color[$key]['color'] ?? false;
    }

    public static function get_colors($key = false) {
        $colors = self::COLORS;
        uasort($colors, function($a, $b) {
            if ($a['label'] == $b['label']) {
                return 0;
            }
            return ($a['label'] < $b['label']) ? -1 : 1;
        });

        if(!$key) {
            return $colors;
        }

        return array_map(function($color) use ($key) {
            return $color[$key];
        }, $colors);
    }

}
