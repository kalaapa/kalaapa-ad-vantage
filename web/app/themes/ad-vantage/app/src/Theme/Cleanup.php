<?php

namespace App\Theme;

use function Sober\Intervention\intervention;

class Cleanup {

    public function __construct() {
        add_action('init', function() {
            $this->disable_head_links();
            $this->disable_emoji();
            $this->disable_feeds();
            $this->disable_embed();
            $this->clean_rewrite_rules();
            $this->clean_resource_hints();
        });
    }

    public function disable_embed() {

        // Remove the REST API endpoint.
        // remove_action( 'rest_api_init', 'wp_oembed_register_route' );
        // Turn off oEmbed auto discovery.
        // add_filter( 'embed_oembed_discover', '__return_false' );
        // Don't filter oEmbed results.
        // remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
        // Remove oEmbed discovery links.
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );
        // add_filter( 'tiny_mce_plugins', function($plugins) {
        //     return array_diff($plugins, ['wpembed']);
        // });
        // Remove all embeds rewrite rules.
        // add_filter( 'rewrite_rules_array', function($rules) {
        //     foreach($rules as $rule => $rewrite) {
        //         // if(false !== strpos($rewrite, 'embed=true')) {
        //         //     unset($rules[$rule]);
        //         // }
        //     }
        //     return $rules;
        // });
        // Remove filter of the oEmbed result before any HTTP requests are made.
        // remove_filter( 'pre_oembed_result', 'wp_filter_pre_oembed_result', 10 );

        add_action( 'wp_footer', function() {
            wp_dequeue_script( 'wp-embed' );
        });
    }

    public function disable_head_links() {
        remove_action( 'wp_head', 'rsd_link' );
        remove_action( 'wp_head', 'wlwmanifest_link' );
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
        remove_action( 'wp_head', 'wp_generator' );
        remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    }

    public function clean_resource_hints() {
        add_filter('wp_resource_hints', function($urls) {
            foreach($urls as $k => $url) {
                if(false === strpos($url, 's.w.org')) {
                    continue;
                }
                unset($urls[$k]);
            }
            return $urls;
        });
    }

    public function disable_feeds() {
        remove_action( 'wp_head', 'rest_output_link_wp_head', 10);
        remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
        remove_action( 'wp_head', 'feed_links', 2 );
    }

    public function disable_emoji() {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );
        remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
        remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

        add_filter( 'tiny_mce_plugins', function( $plugins ) {
            return is_array( $plugins ) ? array_diff( $plugins, ['wpemoji'] ) : [];
        });
        add_filter( 'wp_resource_hints', function( $urls, $relation_type ) {
            if ( 'dns-prefetch' == $relation_type ) {
                /** This filter is documented in wp-includes/formatting.php */
                $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

                $urls = array_diff( $urls, array( $emoji_svg_url ) );
            }
            return $urls;
        }, 10, 2 );
    }

    public function clean_rewrite_rules() {
        add_filter( 'rewrite_rules_array', function($rules) {
            if(!is_array($rules)) {
                return $rules;
            }
            foreach($rules as $regex => $query) {
                if( preg_match('/feed|attachment|author|search|trackback/', $regex) ) {
                    unset($rules[$regex]);
                }
                if( preg_match('/feed|attachment|author|search|trackback/', $query) ) {
                    unset($rules[$regex]);
                }
            }
            return $rules;
        });
    }

}
