<?php

namespace App\Theme;

use App\PostTypes;

class Breadcrumb {

    public function __construct() {
        add_filter('inc2734_wp_breadcrumbs_main_taxonomy', [$this, 'set_main_taxonomy'], 10, 3);
    }

    public function set_main_taxonomy( $taxonomy, $taxonomies, $post_type_object ) {
        if($post_type_object->name === PostTypes::POST_TYPE_JOB) {
            return false;
        }
        return $post_type_object->primary_taxonomy ?? $taxonomy;
    }

}
