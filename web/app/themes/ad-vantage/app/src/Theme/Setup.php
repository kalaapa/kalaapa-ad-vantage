<?php

namespace App\Theme;

class Setup {

    private $theme_support = [
        'title-tag',
        'post-thumbnails',
        'html5' => [
            'search-form',
            'gallery',
            'caption',
        ],
        'responsive-embeds',
        'editor-styles',
        'disable-custom-colors',
        'disable-custom-font-sizes',
    ];

    private $menus = [
        'header' => 'Header',
        'footer' => 'Footer',
    ];

    public function __construct() {
        add_action( 'after_setup_theme', [ $this, 'theme_setup' ] );
    }

    /**
     * Theme setup
     */
    public function theme_setup() {

        $this->register_menus();

        add_filter('jpeg_quality', function($quality) {
            return 100;
        });

        $this->add_support();

        if( is_admin() ) {
            $editor_stylesheet = \App\container()->get('assets.packages')->getUrl('css/editor-style.css');
            add_editor_style($editor_stylesheet);
            add_post_type_support('page', 'excerpt');
        }

        load_theme_textdomain('nactalia', get_template_directory() . '/languages');
    }

    /**
     * Theme support
     */
    private function add_support() {
        foreach ( $this->theme_support as $name => $support ) {
            if ( is_array( $support ) ) {
                add_theme_support( $name, $support );
            } else {
                add_theme_support( $support );
            }
        }
    }

    private function register_menus() {
        register_nav_menus($this->menus);
    }

}
