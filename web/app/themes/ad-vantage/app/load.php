<?php

namespace App;

use Timber\Timber;
use Timber\Menu;
use StoutLogic\AcfBuilder\FieldsBuilder;
use App\PostTypes;
use Inc2734\WP_Breadcrumbs\Bootstrap;
use Twig_Function;
use Timber\Helper;

require 'app.php';

// Setup
add_action('after_setup_theme', function () {

    new Theme\Assets();
    new Theme\Setup();
    new Theme\Cleanup();
    new Theme\WhiteLabel();
    new Theme\Breadcrumb();
    new Theme\Seo();

    new Schema\Job();
    new Schema\Breadcrumb();

    new Acf\Embed();
    new Acf\GoogleMap();

    new Admin\Options();
    new Admin\Cleanup();
    new Admin\Editor();

    new PostTypes();

}, 0);


// Timber
Timber::$cache = !WP_DEBUG;
// Template::$autoescape = 'html';
\Timber\Timber::$dirname = array_merge((array) \Timber\Timber::$dirname, ['assets']);
$timber = new Timber();

// Grab the root domain before polylang changes URLs
// timber/context hook happens too late
$root_home_url = home_url();
$root_site_url = site_url();

$options = [];
$fields = collect(glob(get_theme_file_path() . '/app/src/Fields/*.php'));
add_action('init', function() use ($fields, &$options) {
    $fields->map(function ($field) {
        return require_once($field);
    })->map(function ($field) {
        if ($field instanceof FieldsBuilder) {
            if(!function_exists('acf_add_local_field_group')) {
                return;
            }
            acf_add_local_field_group($field->build());
        }
    });

    // Extract options from fields
    $field_groups = $fields->filter(function($field) {
        return false !== strpos($field, 'options_global_');
    })->map(function ($field) {
        return require($field);
    });

    // Get option fields
    foreach($field_groups as $fields) {
        foreach($fields->getFields() as $field) {
            $options[] = $field;
        }
    }
});

add_filter('timber/context', function ( $context ) use(&$options) {

    $context['root_home_url'] = container()->get('paths.home_root_url');

    // Add trailingslash to home URL
    $context['site']->home_url = trailingslashit($context['site']->home_url);

    // Breacrumb
    $context['breadcrumbs'] = \App\container()->get('app.breadcrumb');

    // Menus
    foreach (['header', 'footer'] as $menu) {
        $context['nav_' . $menu] = false;
        if( !has_nav_menu( $menu ) ) {
            continue;
        }
        $context['nav_' . str_replace('-', '_', $menu)] = new Menu( $menu );

    }

    // Options
    if( function_exists('get_field') ) {
        foreach($options as $field) {
            $context['options'][$field->getName()] = get_field($field->getName(), 'option');
        }
    }
    $context['options']['privacy_policy_url'] = get_privacy_policy_url();

    if( is_singular('post') || is_category() ) {
        $message_context = 'post';
    } elseif( is_post_type_archive('product') || is_singular('product') ) {
        $message_context = 'product';
    } else {
        $message_context = 'page';
    }

    return $context;
});

add_filter('app/js/args', function($args) {
    global $wp_locale;
    return [
        'google_analytics_id' => get_option('options_google_analytics_id'),
    ];
});

if(WP_DEBUG) {
    add_filter('show_admin_bar', '__return_false');
}

add_action('timber/twig/functions', function($twig) {
    $twig->addFunction(new Twig_Function('AppImage', function( $pid = false, $ImageClass = 'App\Entity\Image' ) {
        if ( is_array($pid) && !Helper::is_array_assoc($pid) ) {
            foreach ( $pid as &$p ) {
                $p = new $ImageClass($p);
            }
            return $pid;
        }
        return new $ImageClass($pid);
    } ));

    return $twig;
});
