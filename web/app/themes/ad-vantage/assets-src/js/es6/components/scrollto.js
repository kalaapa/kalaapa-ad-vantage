import MoveTo from 'moveto';
import SelectorEngine from 'bootstrap/js/dist/dom/selector-engine';
import { makeArray } from '../util';

const moveTo = new MoveTo();

(() => {

  const triggers = makeArray(SelectorEngine.find('[data-scroll]'));

  triggers.forEach(trigger => {
    moveTo.registerTrigger(trigger);
  });

})();
