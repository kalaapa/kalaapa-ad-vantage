export default class Nav {

  /**
   * Init
   */
  static init() {

    const self = new Nav();

    self.nav = document.querySelector('.nav');
    if (!self.nav) {
      return;
    }

    self.navBurger = self.nav.querySelector('.js-nav-burger');
    self.navOverlay = self.nav.querySelector('.js-nav-overlay');
    self.navItems = Array.prototype.slice.call(self.nav.querySelectorAll('.js-nav-menu-item-has-submenu'));

    self.binds();

  }

  /**
   * Bind events
   */
  binds() {
    // Nav < LG
    this.navBurger && this.navBurger.addEventListener('click', this.toggleMenu, false);
    this.navOverlay && this.navOverlay.addEventListener('click', this.toggleMenu, false);

    // Nav >= LG
    this.navItems.forEach(item => {
      item.addEventListener('mouseenter', (e) => {
        this.toggleSubMenu(e, item);
      }, false);
      item.addEventListener('mouseleave', (e) => {
        this.toggleSubMenu(e, item);
      }, false);
    });
  }

  toggleMenu() {
    this.classList.toggle('active');
    document.documentElement.classList.toggle('nav--open');
  }

  closeAllSubMenus(current = false) {
    this.navItems.forEach(item => {
      const subMenu = item.querySelector('.js-nav-submenu');
      if (!subMenu || (current && subMenu === current)) {
        return;
      }
      subMenu.classList.remove('active');
    });
  }

  toggleSubMenu(e, item) {
    // const subMenu = item.querySelector('.js-nav-submenu');
    // // e.preventDefault();

    // if (!subMenu) {
    //   return;
    // }

    // Close all other submenus
    // this.closeAllSubMenus(subMenu);

    item.classList.toggle('active', !item.classList.contains('active') && e.type === 'mouseenter');
  }

  clickOutside() {
    let targetElement = e.target;
    do {
      if (targetElement == currentTarget) {
        return;
      }
      targetElement = targetElement.parentNode;
    } while (targetElement);
    this.closeAllSubMenus();
  }

}
