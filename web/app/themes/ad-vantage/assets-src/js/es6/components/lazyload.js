import 'lazysizes';
import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes/plugins/respimg/ls.respimg';

// Lazysizes config
window.lazySizesConfig = window.lazySizesConfig || {};
lazySizesConfig.loadMode = 1;
