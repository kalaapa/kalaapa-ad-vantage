import bsCustomFileInput from 'bs-custom-file-input';
bsCustomFileInput.init()

import './components/lazyload';
import './components/forms';
import './components/scrollto';
import './components/cookie';
import Nav from './components/nav';
Nav.init();
