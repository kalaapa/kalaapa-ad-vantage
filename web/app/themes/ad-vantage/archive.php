<?php

use App\PostTypes;

$context = Timber::get_context();

$context['title'] = get_the_archive_title();
if(function_exists('the_seo_framework')) {
    $tsf = the_seo_framework();
    $context['title'] = $tsf->get_generated_archive_title();
}

if( is_post_type_archive() || is_tax() ) {
    if( function_exists('get_field') ) {
        $context['job_cover'] = get_field('job_cover', 'options');
        $context['job_content'] = get_field('job_content', 'options');
    }
    $context['filters'] = Timber::get_terms([
        'taxonomy' => [PostTypes::TAXONOMY_CITY, PostTypes::TAXONOMY_SKILL],
        'hide_empty' => true,
    ]);
}
$context['term'] = false;
if( is_tax() ) {
    $context['term'] = new Timber\Term();
}

$context['jobs_url'] = get_post_type_archive_link(PostTypes::POST_TYPE_JOB);

Timber::render('archive.html.twig', $context);
