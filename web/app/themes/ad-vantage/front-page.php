<?php

$context = Timber::get_context();

if( is_front_page() && is_home() ) {
    $context['posts'] = new Timber\PostQuery();
} else {
    $context['post'] = new Timber\Post();
}

Timber::render('home.html.twig', $context );
