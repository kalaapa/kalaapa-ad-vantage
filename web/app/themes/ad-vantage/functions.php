<?php

require 'app/vendor/autoload.php';

add_filter('wpcf7_load_js', '__return_false');
add_filter('wpcf7_load_css', '__return_false');

add_action('nav_menu_css_class', function ($classes, $item) {

    if( !is_singular() && !is_front_page() ) {
        return $classes;
    }

	// Getting the current post details
	$post = get_queried_object();
	if (isset($post->post_type)) {
		if ($post->post_type == 'post') {
			$current_post_type_slug = get_permalink(get_option('page_for_posts'));
		} else {
			// Getting the post type of the current post
			$current_post_type = get_post_type_object(get_post_type($post->ID));
			$current_post_type_slug = $current_post_type->rewrite['slug'];
        }

        if(!$current_post_type_slug) {
            return $classes;
        }

		// Getting the URL of the menu item
		$menu_slug = strtolower(trim($item->url));

		// If the menu item URL contains the current post types slug add the current-menu-item class
		if (strpos($menu_slug, $current_post_type_slug) !== false) {
			$classes[] = 'current-menu-item';
		}
	}
	// Return the corrected set of classes to be added to the menu item
	return $classes;
}, 10, 2);
