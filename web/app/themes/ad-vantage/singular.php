<?php

$context = array_merge(Timber::get_context(), isset($context) ? $context : []);

$context['post'] = new Timber\Post();
$template = pathinfo(get_page_template_slug(), PATHINFO_FILENAME);

if( is_singular(App\PostTypes::POST_TYPE_JOB) ) {
    $context['form'] = new App\Forms\JobForm();
}

Timber::render([
    sprintf('%s.html.twig', $template),
    sprintf('%s.html.twig', $context['post']->post_type),
    'singular.html.twig',
], $context );
